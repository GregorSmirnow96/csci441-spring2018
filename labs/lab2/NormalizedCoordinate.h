#include "Coordinate.h";

class NormalizedCoordinate
{
	public:
		float x;
		float y;

	NormalizedCoordinate(
		float x,
		float y) :
			x(x),
			y(y)
	{
		
	}

	NormalizedCoordinate(
		int x,
		int y) :
		x(x),
		y(y)
	{

	}

	NormalizedCoordinate(
		Coordinate* coordinate,
		int maxWidth,
		int maxHeight)
	{
		Coordinate coord = *coordinate;
		x =	-1 + coord.x * 2 / (float)maxWidth;
		y = 1 - coord.y * 2 / (float)maxHeight;
	}


	~NormalizedCoordinate()
	{

	}
};
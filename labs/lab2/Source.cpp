#include <glad\glad.h>
#include <GLFW\glfw3.h>
#include <iostream>
#include <fstream>
#include <sstream>
#include "NormalizedCoordinate.h"
using namespace std;

void framebuffer_size_callback(
	GLFWwindow* window,
	int width,
	int height);
void processInput(GLFWwindow *window);
std::string shaderTypeName(GLenum shaderType);
std::string readFile(const std::string& fileName);
GLuint createShader(
	const std::string& fileName,
	GLenum shaderType);
GLuint createShaderProgram(
	GLuint vertexShader,
	GLuint fragmentShader);

int main()
{
	cout << "Enter 3 points (enter a point as x y r g b):" << endl;
	float x[3];
	float y[3];
	float r[3];
	float g[3];
	float b[3];
	cin >> x[0] >> y[0] >> r[0] >> g[0] >> b[0];
	cin >> x[1] >> y[1] >> r[1] >> g[1] >> b[1];
	cin >> x[2] >> y[2] >> r[2] >> g[2] >> b[2];

	Coordinate* coord1 = new Coordinate(x[0], y[0]);
	Coordinate* coord2 = new Coordinate(x[1], y[1]);
	Coordinate* coord3 = new Coordinate(x[2], y[2]);
	NormalizedCoordinate* nc1 = new NormalizedCoordinate(coord1, 800, 600);
	NormalizedCoordinate* nc2 = new NormalizedCoordinate(coord2, 800, 600);
	NormalizedCoordinate* nc3 = new NormalizedCoordinate(coord3, 800, 600);

	glfwInit();
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

	GLFWwindow* window = glfwCreateWindow(800, 600, "LearnOpenGL", NULL, NULL);
	if (window == NULL)
	{
		std::cout << "Failed to create GLFW window" << std::endl;
		glfwTerminate();
		return -1;
	}
	glfwMakeContextCurrent(window);

	if (!gladLoadGLLoader((GLADloadproc)glfwGetProcAddress))
	{
		std::cout << "Failed to initialize GLAD" << std::endl;
		return -1;
	}

	glViewport(0, 0, 800, 600);
	glfwSetFramebufferSizeCallback(window, framebuffer_size_callback);
		
	float vertices[] = {
		nc1->x, nc1->y, 0.0f, r[0], g[0], b[0],
		nc2->x, nc2->y, 0.0f, r[1], g[1], b[1],
		nc3->x, nc3->y, 0.0f, r[2], g[2], b[2]
	};

	GLuint VBO;
	glGenBuffers(
		1,
		&VBO);

	GLuint VAO;
	glGenVertexArrays(
		1,
		&VAO);

	glBindBuffer(
		GL_ARRAY_BUFFER,
		VBO);

	glBufferData(
		GL_ARRAY_BUFFER,
		sizeof(vertices),
		vertices,
		GL_STATIC_DRAW);

	glBindVertexArray(VAO);
	glBindBuffer(
		GL_ARRAY_BUFFER,
		VBO);
	glBufferData(
		GL_ARRAY_BUFFER,
		sizeof(vertices),
		vertices,
		GL_STATIC_DRAW);

	glVertexAttribPointer(
		0,
		3,
		GL_FLOAT,
		GL_FALSE,
		6 * sizeof(float),
		(void*)0);
	glEnableVertexAttribArray(0);

	glVertexAttribPointer(
		1,
		3,
		GL_FLOAT,
		GL_FALSE,
		6 * sizeof(float),
		(void*)(3 * sizeof(float)));
	glEnableVertexAttribArray(1);

	GLuint vertexShader = createShader(
		"VertexShaderText.glsl",
		GL_VERTEX_SHADER);
	
	GLuint fragmentShader = createShader(
		"FragmentShaderText.glsl",
		GL_FRAGMENT_SHADER);

	GLuint shaderProgram = createShaderProgram(
		vertexShader,
		fragmentShader);

	glDeleteShader(vertexShader);
	glDeleteShader(fragmentShader);

	while (!glfwWindowShouldClose(window))
	{
		processInput(window);

		glClearColor(0.2f, 0.3f, 0.3f, 1.0f);
		glClear(GL_COLOR_BUFFER_BIT);

		glUseProgram(shaderProgram);
		glBindVertexArray(VAO);
		glDrawArrays(
			GL_TRIANGLES,
			0,
			3);

		glfwSwapBuffers(window);
		glfwPollEvents();
	}

	glfwTerminate();
	return 0;
}

void framebuffer_size_callback(GLFWwindow* window, int width, int height)
{
	glViewport(0, 0, width, height);
}

void processInput(GLFWwindow *window)
{
	if (glfwGetKey(window, GLFW_KEY_ESCAPE) == GLFW_PRESS)
		glfwSetWindowShouldClose(window, true);
}

string readFile(const std::string& fileName) {
	ifstream stream(fileName);
	std::stringstream buffer;
	buffer << stream.rdbuf();

	std::string source = buffer.str();
	std::cout << "Source:" << std::endl;
	std::cout << source << std::endl;

	return source;
}

GLuint createShader(
	const std::string& fileName,
	GLenum shaderType)
{
	string source = readFile(fileName);
	const char* src_ptr = source.c_str();

	GLuint shader;

	if (shaderTypeName(shaderType) == "VERTEX")
	{
		shader = glCreateShader(GL_VERTEX_SHADER);
	}
	else
	{
		shader = glCreateShader(GL_FRAGMENT_SHADER);
	}

	string compiledShaderString =
		readFile(fileName.c_str());

	const char* shaderSource = compiledShaderString.c_str();

	glShaderSource(
		shader,
		1,
		&shaderSource,
		NULL);
	glCompileShader(shader);

	int success;
	char infoLog[512];
	glGetShaderiv(shader, GL_COMPILE_STATUS, &success);
	if (!success) {
		glGetShaderInfoLog(shader, 512, NULL, infoLog);
		std::cerr << "ERROR::SHADER::" << shaderTypeName(shaderType)
			<< "::COMPILATION_FAILED\n"
			<< infoLog << std::endl;
	}

	return shader;
}

std::string shaderTypeName(GLenum shaderType) {
	switch (shaderType) {
	case GL_VERTEX_SHADER: return "VERTEX";
	case GL_FRAGMENT_SHADER: return "FRAGMENT";
	default: return "UNKNOWN";
	}
}

GLuint createShaderProgram(
	GLuint vertexShader,
	GLuint fragmentShader)
{
	GLuint shaderProgram;
	shaderProgram = glCreateProgram();

	glAttachShader(
		shaderProgram,
		vertexShader);
	glAttachShader(
		shaderProgram,
		fragmentShader);
	glLinkProgram(shaderProgram);

	glUseProgram(shaderProgram);
	
	int success;
	glGetProgramiv(shaderProgram, GL_LINK_STATUS, &success);
	if (!success) {
		char infoLog[512];
		glGetProgramInfoLog(shaderProgram, 512, NULL, infoLog);
		std::cerr << "ERROR::PROGRAM::COMPILATION_FAILED\n"
			<< infoLog << std::endl;
	}

	return shaderProgram;
}

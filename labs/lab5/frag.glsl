#version 330 core

in vec3 ourPos;
in vec3 ourColor;
in vec3 ourNorm;
out vec4 fragColor;

uniform vec3 lightPos;
uniform vec3 lightIntensity;
uniform vec3 eyePos;
uniform vec3 ambientLight;
uniform int phongPow;

vec3 calculateReflection()
{
	vec3 r = -lightPos + 2 * (dot(lightPos, ourNorm)) * ourNorm;
	return normalize(r);
}

vec3 calculateNewColor()
{
	vec3 norm = normalize(ourNorm);
	vec3 normalizedLight = normalize(lightPos - ourPos);

	float diff = max(dot(ourNorm, normalizedLight), 0.0);
	vec3 diffuse = diff * lightIntensity;

	float ambientStrength = 0.25;
	vec3 ambient = ambientStrength * lightIntensity;

	vec3 reflectionVector = calculateReflection();
	vec3 phongShimmer = lightIntensity * pow(max(0, dot(normalize(eyePos), reflectionVector)), phongPow);

	vec3 result = (ambient + diffuse) * ourColor + phongShimmer;

	return result;
}	

void main()
{
    fragColor = vec4(calculateNewColor(), 1.0);
}

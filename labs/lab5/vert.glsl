#version 330 core
layout (location = 0) in vec3 aPos;
layout (location = 1) in vec3 aColor;
layout (location = 2) in vec3 aNorm;

out vec3 ourPos;
out vec3 ourColor;
out vec3 ourNorm;
out mat4 transformationMatrix;

uniform mat4 modelMatrix;
uniform mat4 cameraTransformMatrix;
uniform int projectionMode;

vec4 getNewPos(mat4 m)
{
	if(projectionMode == 0){
		vec4 pos = vec4(aPos, 1);
		float x = m[0][0] * pos[0] + m[0][1] * pos[1] + m[0][2] * pos[2] + m[0][3] * pos[3];
		float y = m[1][0] * pos[0] + m[1][1] * pos[1] + m[1][2] * pos[2] + m[1][3] * pos[3];
		float z = m[2][0] * pos[0] + m[2][1] * pos[1] + m[2][2] * pos[2] + m[2][3] * pos[3];
		return vec4(x, y, z, 1);
	}
	if(projectionMode == 1){
		vec4 pos = vec4(aPos, 1);
		float x = m[0][0] * pos[0] + m[0][1] * pos[1] + m[0][2] * pos[2] + m[0][3] * pos[3];
		float y = m[1][0] * pos[0] + m[1][1] * pos[1] + m[1][2] * pos[2] + m[1][3] * pos[3];
		float z = m[2][0] * pos[0] + m[2][1] * pos[1] + m[2][2] * pos[2] + m[2][3] * pos[3];
		float w = m[3][0] * pos[0] + m[3][1] * pos[1] + m[3][2] * pos[2] + m[3][3] * pos[3];
		return vec4(x, y, z, w);
	}
}

mat4 multiplyMatrices(mat4 m1, mat4 m2)
{
	mat4 matrix;
	for (int i = 0; i < 4; i++)
	{
		for (int j = 0; j < 4; j++)
		{
			float matrixEntry = 0;
			for (int k = 0; k < 4; k++)
			{
				float elementOne = m1[i][k];
				float elementTwo = m2[k][j];
				matrixEntry += elementOne * elementTwo;
			}
			matrix[i][j] = matrixEntry;
		}
	}
	return matrix;
}

void main()
{
	transformationMatrix = cameraTransformMatrix * modelMatrix;
	vec4 transformedPos = transformationMatrix * vec4(aPos, 1);
	gl_Position = vec4(transformedPos);
	ourColor = aColor;

	//ourNorm = aNorm;

	mat4 m = transpose(inverse(transformationMatrix));
	vec4 shiftedNormWithW = m * vec4(aNorm, 1);
	vec3 shiftedNorm = vec3(shiftedNormWithW[0], shiftedNormWithW[1], shiftedNormWithW[2]);
	ourNorm = shiftedNorm;
}
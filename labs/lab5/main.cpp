#include <iostream>
#include <string>
#include <sstream>
#include <fstream>
#include <cmath>

#include <glad/glad.h>
#include <GLFW/glfw3.h>
#include <Camera.h>
#include <Controller.h>
#include <Matrix.h>
#include <MatrixTypes.h>
#include <Primitive3D.h>
#include <PrimitiveTypes3D.h>

#include <csci441/shader.h>

const int SCREEN_WIDTH = 640;
const int SCREEN_HEIGHT = 480;
const int PHONG_POW = 4096;
const GLfloat LIGHT_X = 1.0f;
const GLfloat LIGHT_Y = 0.0f;
const GLfloat LIGHT_Z = 0.0f;

void updateModelToProjectionMatrix(
	Shader shader,
	Controller controller);
void updateEyePosition(
	Shader shader,
	Controller controller);
void updateModelMatrix(
	Shader shader,
	Controller controller);
void updateProjectionMode(
	Shader shader,
	int mode);
void createLightSource(Shader shader);
void updatePhongPow(Shader shader);
Primitive* changePrimitive(Controller controller);

void framebufferSizeCallback(GLFWwindow* window, int width, int height) {
	glViewport(0, 0, width, height);
}

void processInput(GLFWwindow *window, Shader &shader) {
	if (glfwGetKey(window, GLFW_KEY_ESCAPE) == GLFW_PRESS) {
		glfwSetWindowShouldClose(window, true);
	}
}

void errorCallback(int error, const char* description) {
	fprintf(stderr, "GLFW Error: %s\n", description);
}

int main(void) {
	GLFWwindow* window;

	glfwSetErrorCallback(errorCallback);

	/* Initialize the library */
	if (!glfwInit()) { return -1; }

#ifdef __APPLE__
	glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
#endif

	/* Create a windowed mode window and its OpenGL context */
	window = glfwCreateWindow(SCREEN_WIDTH, SCREEN_HEIGHT, "Shading Lab", NULL, NULL);
	if (!window) {
		glfwTerminate();
		return -1;
	}

	/* Make the window's context current */
	glfwMakeContextCurrent(window);

	// tell glfw what to do on resize
	glfwSetFramebufferSizeCallback(window, framebufferSizeCallback);

	// init glad
	if (!gladLoadGL()) {
		std::cerr << "Failed to initialize OpenGL context" << std::endl;
		glfwTerminate();
		return -1;
	
	}

	// define VBO and VAO
	GLuint VBO;
	glGenBuffers(1, &VBO);
	GLuint VAO;
	glGenVertexArrays(1, &VAO);

	// create the shaders
	Shader shader("vert.glsl", "frag.glsl");

	// setup the textures
	shader.use();

	// and use z-buffering
	glEnable(GL_DEPTH_TEST);

	// initialize light source
	createLightSource(shader);

	// pass Phong POW!!!!!
	updatePhongPow(shader);

	// initialize the model and controller
	Primitive* primitive = new Primitive();

	//primitive->verticeRepository->print();
	Controller controller;
	primitive->copyAndDescribeVertex(VBO, VAO);

	/* Loop until the user closes the window */
	while (!glfwWindowShouldClose(window)) {
		// process input
		processInput(window, shader);

		/* Render here */
		glClearColor(0.2f, 0.3f, 0.3f, 1.0f);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		// activate shader
		shader.use();

		// read user input
		controller.recieveUserInput();

		// change primitive if necessary
		if (controller.primitiveChanged)
		{
			controller.primitiveChanged = false;
			primitive = changePrimitive(controller);
			primitive->copyAndDescribeVertex(VBO, VAO);
		}

		// create transformation matrix and pass it to shader
		updateProjectionMode(shader, controller.projectionMode);
		updateModelMatrix(shader, controller);
		updateModelToProjectionMatrix(shader, controller);
		updateEyePosition(shader, controller);

		// render the primitive
		primitive->render(VAO);

		/* Swap front and back and poll for io events */
		glfwSwapBuffers(window);
		glfwPollEvents();

		Sleep(17);
	}

	glfwTerminate();
	return 0;
}

void createLightSource(Shader shader)
{
	GLint shaderVariableIndex =
		glGetUniformLocation(
			shader.id(),
			"lightPos");
	GLfloat lightDirection[] =
	{
		LIGHT_X,
		LIGHT_Y,
		LIGHT_Z
	};
	glUniform3f(
		shaderVariableIndex,
		lightDirection[0],
		lightDirection[1],
		lightDirection[2]); 

	shaderVariableIndex =
		glGetUniformLocation(
			shader.id(),
			"lightIntensity");
	GLfloat lightIntensity[] = { 1.0f, 1.0f, 1.0f };
	glUniform3f(
		shaderVariableIndex,
		lightIntensity[0],
		lightIntensity[1],
		lightIntensity[2]);
}

void updateEyePosition(
	Shader shader,
	Controller controller)
{
	GLint shaderVariableIndex =
		glGetUniformLocation(
			shader.id(),
			"eyePos");
	GLfloat eyePosition[] = {
		controller.cameraLocationX,
		controller.cameraLocationY,
		controller.cameraLocationZ};
	glUniform3f(
		shaderVariableIndex,
		eyePosition[0],
		eyePosition[1],
		eyePosition[2]);
}

void updateProjectionMode(
	Shader shader,
	int mode)
{
	//mode = 0 => orthographic
	//mode = 1 => perspective
	GLint shaderVariableIndex =
		glGetUniformLocation(
			shader.id(),
			"projectionMode");
	glUniform1i(
		shaderVariableIndex,
		mode);
}


void updateModelToProjectionMatrix(
	Shader shader,
	Controller controller)
{
	OrthographicMatrix orthMatrix(-1.0f, 1.0f, -1.0f, 1.0f, 1.0f, 50.0f);
	PerspectiveMatrix perspMatrix(-1.0f, 1.0f, -1.0f, 1.0f, 10.0f, 50.0f);
	TranslationMatrix eyeSpaceMatrix(0.0f, 0.0f, controller.cameraLocationZ);

	Matrix* projection;
	if (controller.projectionMode == 0) {
		Matrix m = orthMatrix;
		projection = &m;
	}
	else {
		Matrix m = perspMatrix;
		projection = &m;
	}
	Matrix n = *projection * eyeSpaceMatrix;

	n.passToShader(
		shader,
		"cameraTransformMatrix");
}

void updateModelMatrix(
	Shader shader,
	Controller controller)
{
	TranslationMatrix translateMatrix(
		controller.modelTranslateX,
		controller.modelTranslateY,
		controller.modelTranslateZ);
	ScaleMatrix scaleMatrix(
		controller.modelScale,
		controller.modelScale,
		controller.modelScale);
	RotationMatrixX x(controller.xRotationDegree);
	RotationMatrixY y(controller.yRotationDegree);
	RotationMatrixZ z(controller.zRotationDegree);

	Matrix* m = &(y * x);
	m = &(z * *m);
	m = &(scaleMatrix * *m);
	m = &(translateMatrix * *m);

	m->passToShader(
		shader,
		"modelMatrix");
}

void updatePhongPow(Shader shader)
{
	GLint shaderVariableIndex =
		glGetUniformLocation(
			shader.id(),
			"phongPow");
	GLint phongPow = PHONG_POW;
	glUniform1i(
		shaderVariableIndex,
		phongPow);
}

Primitive* changePrimitive(Controller controller)
{
	if (controller.primitiveIndex == 0)
	{
		return new Primitive();
	}
	else if (controller.primitiveIndex == 1)
	{
		return new Cylinder(0.25f, 0.7f, 256);
	}
	else if (controller.primitiveIndex == 2)
	{
		return new Cone(1.0f, 0.4f, 100);
	}
}
#ifndef CONTROLLER_H
#define CONTROLLER_H

class Controller
{
	public:
		string primitiveType;

		Controller() : primitiveType("Circle") { }

		~Controller() {}

		void recieveUserInput()
		{
			int primitiveChange = GetAsyncKeyState(VK_SPACE);

			if (primitiveChange)
			{
				if (primitiveType == "Circle")
				{
					primitiveType = "Square";
				}
				else if (primitiveType == "Square")
				{
					primitiveType = "Circle";
				}
				Sleep(500);
			}
		}
};

#endif
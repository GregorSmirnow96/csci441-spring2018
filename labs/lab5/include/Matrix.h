#ifndef MATRIX_H
#define MATRIX_H

#include <iostream>

#include <glad/glad.h>
#include <csci441/shader.h>

class Matrix
{
	public:
		int height;
		int width;
		GLfloat * elements;

		Matrix() {}

		Matrix(
			int inHeight,
			int inWidth)
			: height(inHeight),
			width(inWidth),
			elements(new float[width * height]) {}

		~Matrix() {}

		friend std::ostream& operator<<(
			std::ostream& stream,
			const Matrix& m);

		void populate(float inElements[])
		{
			for (int i = 0; i < height; i++) {
				for (int j = 0; j < width; j++) {
					elements[j + (i * width)]
						= inElements[j + (i * width)];
				}
			}
		}

		GLfloat getElementAt(
			int iIndex,
			int jIndex)
		{
			return elements[jIndex + (iIndex * width)];
		}

		void setElementAt(
			int iIndex,
			int jIndex,
			GLfloat newValue)
		{
			elements[jIndex + (iIndex * width)] = newValue;
		}

		void passToShader(
			Shader shader,
			const char* shaderVariableName)
		{
			GLint shaderVariableIndex =
				glGetUniformLocation(
					shader.id(),
					shaderVariableName);
			glUniformMatrix4fv(
				shaderVariableIndex,
				1,
				GL_FALSE,
				elements);
		}
};

Matrix operator*(Matrix& m1, Matrix& m2)
{
	int sizeOfNewMatrix = m1.height * m2.width;
	Matrix matrix(m1.height, m2.width);
	GLfloat* elements = new GLfloat[sizeOfNewMatrix];
	for (int i = 0; i < m1.height; i++)
	{
		for (int j = 0; j < m2.width; j++)
		{
			GLfloat matrixEntry = 0;
			for (int k = 0; k < m1.width; k++)
			{
				GLfloat elementOne = m1.getElementAt(i, k);
				GLfloat elementTwo = m2.getElementAt(k, j);
				matrixEntry += elementOne * elementTwo;
			}
			elements[j + (i * m2.width)] = matrixEntry;
		}
	}
	matrix.populate(elements);
	delete elements;

	return matrix;
}

ostream& operator<<(ostream& stream, Matrix& matrix)
{
	for (int i = 0; i < matrix.height; i++) {
		stream << "[ ";
		for (int j = 0; j < matrix.width; j++) {
			stream << matrix.getElementAt(i, j) << " ";
		}
		stream << "]" << endl;
	}

	return stream;
}

#endif
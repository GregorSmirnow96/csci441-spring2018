#ifndef CREATIONMODES_H
#define CREATIONMODES_H

#include <Primitive.h>
#include <PrimitiveCreationMode.h>
#include <PrimitiveTypes.h>

class CircleMode : public PrimitiveCreationMode
{
	public:
		CircleMode()
		{
			primitive = *(new Circle(0.0f, 0.0f, 0.0f, 3));
			cout << "Circle Mode Instntiated" << endl;
		}

		void updatePrimitive(
			GLFWwindow* window,
			GLfloat screenWidth,
			GLfloat screenHeight,
			GLfloat xPositionInit,
			GLfloat yPositionInit,
			GLuint VBO,
			GLuint VAO)
		{
			glfwGetCursorPos(window, &xPositionFinal, &yPositionFinal);
			GLfloat xDiff = (xPositionFinal - xPositionInit) * 2 / screenWidth;
			GLfloat yDiff = (yPositionFinal - yPositionInit) * 2 / screenHeight;
			GLfloat radius = sqrt(xDiff * xDiff + yDiff * yDiff);
			GLfloat xCenter = xPositionInit * 2 / screenWidth - 1;
			GLfloat yCenter = -yPositionInit * 2 / screenHeight + 1;
			Circle circle(xCenter, yCenter, radius, 30);
			primitive = circle;
		}

		private:
			double xPositionFinal, yPositionFinal;
};

class SquareMode : public PrimitiveCreationMode
{
	public:
		SquareMode()
		{
			primitive = *(new Square(0.0f, 0.0f, 0.0f, 0.0f));
			cout << "Square Mode Instntiated" << endl;
		}

		void updatePrimitive(
			GLFWwindow* window,
			GLfloat screenWidth,
			GLfloat screenHeight,
			GLfloat xPositionInit,
			GLfloat yPositionInit,
			GLuint VBO,
			GLuint VAO)
		{
			glfwGetCursorPos(window, &xPositionFinal, &yPositionFinal);
			GLfloat xInit = xPositionInit * 2 / screenWidth - 1;
			GLfloat xFinal = xPositionFinal * 2 / screenWidth - 1;
			GLfloat yInit = -yPositionInit * 2 / screenHeight + 1;
			GLfloat yFinal = -yPositionFinal * 2 / screenHeight + 1;
			GLfloat left = !(xInit<xFinal) ? xFinal  : xInit;
			GLfloat right = (xInit<xFinal) ? xFinal : xInit;
			GLfloat top = !(yInit<yFinal) ? yFinal : yInit;
			GLfloat bottom = (yInit<yFinal) ? yFinal : yInit;
			Square square(left, right, top, bottom);
			primitive = square;
		}

		private:
			double xPositionFinal, yPositionFinal;
};
#endif
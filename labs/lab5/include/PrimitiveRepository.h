#ifndef PRIMITIVEREPOSITORY_H
#define PRIMITIVEREPOSITORY_H

#include <CreationModeCLL.h>
#include <CreationModes.h>
#include <Primitive.h>
#include <PrimitiveCreationMode.h>
#include <PrimitiveTypes.h>

class PrimitiveRepository
{
	public:
		PrimitiveRepository(
			GLFWwindow* inWindow,
			GLuint inVBO,
			GLuint inVAO) :
			window(inWindow),
			VAO(inVAO),
			VBO(inVBO),
			primitives(new Primitive[128]),
			primitiveCount(0),
			creationMode(new PrimitiveCreationMode()) { }

		~PrimitiveRepository()
		{
		}

		void readPrimitiveCreationInput(
			Controller controller,
			int screenWidth,
			int screenHeight)
		{
			cout << controller.primitiveType << endl;
			glfwGetCursorPos(window, &xPositionInit, &yPositionInit);
			creatingPrimitive = userLeftClickedInsideWindow(screenWidth, screenHeight);
			if (creatingPrimitive)
			{
				int leftButtonPressed = GetAsyncKeyState(VK_LBUTTON);
				while (leftButtonPressed < 0)
				{
					glClearColor(0.2f, 0.3f, 0.3f, 1.0f);
					glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
					creationMode->updatePrimitive(window, screenWidth, screenHeight, xPositionInit, yPositionInit, VBO, VAO, controller.primitiveType);
					Primitive* primitivePointer = &(creationMode->primitive);
					primitivePointer->copyAndDescribeVertex(VBO, VAO);
					primitivePointer->render(VAO);
					renderPrimitives();
					leftButtonPressed = GetAsyncKeyState(VK_LBUTTON);
				}
				creatingPrimitive = false;
				addPrimitive(&(creationMode->primitive));
				renderPrimitives();
			}
		}

		void renderPrimitives()
		{
			for (int i = 0; i < primitiveCount; i++)
			{
				primitives[i].copyAndDescribeVertex(VBO, VAO);
				primitives[i].render(VAO);
			}
			if (primitiveCount > 0 || creatingPrimitive)
			{
				glfwSwapBuffers(window);
			}
		}

		private:
			Primitive * primitives;
			CreationModeCLL creationModeList;
			PrimitiveCreationMode* creationMode;
			GLFWwindow* window;
			GLuint VBO;
			GLuint VAO;
			double xPositionInit, yPositionInit, xPositionFinal, yPositionFinal;
			int primitiveCount = 0;
			bool creatingPrimitive;

			// Takes in screen dimensions.
			// Returns a bool representing whether or not a user clicked inside the window.
			bool userLeftClickedInsideWindow(
				int screenWidth,
				int screenHeight)
			{
				int leftButtonPressed = GetAsyncKeyState(VK_LBUTTON);
				return (leftButtonPressed < 0 &&
						xPositionInit > 0 && xPositionInit < screenWidth &&
						yPositionInit > 0 && yPositionInit < screenHeight);
			}

			// Takes in a pointer to a Primitive.
			// Adds the primitive to PrimitiveRepository's primitives array.
			void addPrimitive(Primitive* primitive)
			{
				primitives[primitiveCount++] = *primitive;
			}

			// Necessary???
			GLfloat* combinePrimitiveVertices()
			{
				int verticesCount = 0;
				for (int i = 0; i < primitiveCount; i++)
				{
					verticesCount += primitives[i].verticesSize;
				}

				GLfloat* combinedVertices = new GLfloat[verticesCount];

				int verticeIndex = 0;
				for (int i = 0; i < primitiveCount; i++)
				{
					Primitive primitive = primitives[i];
					for (int j = 0; j < primitive.verticesSize; j++)
					{
						combinedVertices[verticeIndex++] = primitive.vertices[j];
					}
				}

				return combinedVertices;
			}
};
#endif
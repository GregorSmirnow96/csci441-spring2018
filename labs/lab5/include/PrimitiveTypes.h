#ifndef PRIMITIVETYPES_H
#define PRIMITIVETYPES_H

#include <Primitive.h>

class Square : public Primitive
{
	public:
		GLfloat red = 1.0f;
		GLfloat green = 0.0f;
		GLfloat blue = 0.0f;

		Square(
			GLfloat left,
			GLfloat right,
			GLfloat top,
			GLfloat bottom)
		{
			GLfloat verticeArray[] = {
				left , top   , 0.0f,  red, green, blue,
				right, top   , 0.0f,  red, green, blue,
				left , bottom, 0.0f,  red, green, blue,
				right, top   , 0.0f,  red, green, blue,
				left , bottom, 0.0f,  red, green, blue,
				right, bottom, 0.0f,  red, green, blue
			};
			verticesSize = sizeof(verticeArray);
			vertices = new GLfloat[verticesSize];
			for (int i = 0; i < verticesSize; i++)
			{
				vertices[i] = verticeArray[i];
			}
		}
};

class Circle : public Primitive
{
	public:
		GLfloat red   = 1.0f;
		GLfloat green = 0.0f;
		GLfloat blue  = 0.0f;

		Circle(
			GLfloat centerX,
			GLfloat centerY,
			GLfloat radius,
			int numberOfVertices)
		{
			GLfloat PI = 3.14159;
			GLfloat thetaIncrement = 360.0f / (float) numberOfVertices;
			GLint numberOfTriangles = numberOfVertices - 2;
			GLint numberOfVerticesPerTrianle = 3;
			GLint numberOfFloatsPerVertice = 6;
			verticesSize = numberOfTriangles * numberOfVerticesPerTrianle * numberOfFloatsPerVertice * sizeof(float);
			vertices = new GLfloat[verticesSize];
			GLfloat* verticesX = new GLfloat[numberOfVertices];
			GLfloat* verticesY = new GLfloat[numberOfVertices];
			GLfloat* verticesZ = new GLfloat[numberOfVertices];

			for (int i = 0; i < numberOfVertices; i++)
			{
				GLfloat theta = i * thetaIncrement * PI / 180.0f;
				GLfloat x = centerX + cos(theta) * radius;
				GLfloat y = centerY + sin(theta) * radius;

				verticesX[i] = x;
				verticesY[i] = y;
				verticesZ[i] = 0;
			}

			for (int i = 0; i < numberOfTriangles; i++)
			{
				vertices[i * 18 + 0] = verticesX[0];
				vertices[i * 18 + 1] = verticesY[0];
				vertices[i * 18 + 2] = verticesZ[0];
				vertices[i * 18 + 3] = red;
				vertices[i * 18 + 4] = green;
				vertices[i * 18 + 5] = blue;

				vertices[i * 18 + 6] = verticesX[i + 1];
				vertices[i * 18 + 7] = verticesY[i + 1];
				vertices[i * 18 + 8] = verticesZ[i + 1];
				vertices[i * 18 + 9] = red;
				vertices[i * 18 + 10] = green;
				vertices[i * 18 + 11] = blue;

				vertices[i * 18 + 12] = verticesX[i + 2];
				vertices[i * 18 + 13] = verticesY[i + 2];
				vertices[i * 18 + 14] = verticesZ[i + 2];
				vertices[i * 18 + 15] = red;
				vertices[i * 18 + 16] = green;
				vertices[i * 18 + 17] = blue;}
		}
};
#endif
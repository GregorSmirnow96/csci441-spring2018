#ifndef PRIMITIVE_H
#define PRIMITIVE_H

// Set for 2D graphics (stride is 6) 
class Primitive
{
	public:
		GLfloat* vertices; // stride of 6. vert-vert-vert / color-color-color
		int verticesSize;
	
		Primitive()
		{
			float verticeArray[] = {
				-0.5f, -0.5f, 0.0f,  1.0f, 0.0f, 0.0f,
				0.5f, -0.5f, 0.0f,  0.0f, 0.0f, 1.0f,
				0.5f,  0.5f, 0.0f,  0.0f, 1.0f, 0.0f,
				0.5f,  0.5f, 0.0f,  0.0f, 1.0f, 0.0f,
				-0.5f,  0.5f, 0.0f,  0.0f, 1.0f, 1.0f,
				-0.5f, -0.5f, 0.0f,  1.0f, 0.0f, 0.0f
			};
			verticesSize = sizeof(verticeArray);
			vertices = new float[verticesSize];
			for (int i = 0; i < verticesSize; i++)
			{
				vertices[i] = verticeArray[i];
			}
		}

		~Primitive() { }

		void copyAndDescribeVertex(
			GLuint VBO,
			GLuint VAO)
		{
			// copy vertex data
			glBindBuffer(GL_ARRAY_BUFFER, VBO);
			glBufferData(GL_ARRAY_BUFFER, verticesSize, vertices, GL_DYNAMIC_DRAW);

			// describe vertex layout
			glBindVertexArray(VAO);
			glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 6 * sizeof(float),
				(void*)0);
			glEnableVertexAttribArray(0);
			glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 6 * sizeof(float),
				(void*)(3 * sizeof(float)));
			glEnableVertexAttribArray(1);
		}

		void render(GLuint VAO)
		{
			glBindVertexArray(VAO);
			glDrawArrays(GL_TRIANGLES, 0, verticesSize);
		}

};
#endif
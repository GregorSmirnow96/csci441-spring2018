#ifndef CAMERA_H
#define CAMERA_H

#include <Matrix.h>

class Camera
{
	public:
		Camera(
			GLfloat xCoord,
			GLfloat yCoord,
			GLfloat zCoord)
		{
			positionVector = new Matrix(3, 1);
			positionVector->setElementAt(0, 0, xCoord);
			positionVector->setElementAt(0, 1, yCoord);
			positionVector->setElementAt(0, 2, zCoord);
		}

		~Camera() {
			delete positionVector;
		}

		void move(Matrix translationMatrix)
		{
			positionVector = &(translationMatrix * *positionVector);
		}

		GLfloat getX()
		{
			return positionVector->getElementAt(0, 0);
		}

		GLfloat getY()
		{
			return positionVector->getElementAt(0, 1);
		}

		GLfloat getZ()
		{
			return positionVector->getElementAt(0, 2);
		}

	private:
		Matrix* positionVector;
};

#endif
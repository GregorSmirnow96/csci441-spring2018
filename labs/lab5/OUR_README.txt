We have many custom header files that are required for the program to run.
These are all located in the "include" folder.



CONTROLS:

The controls we implemented for Lab 5 are identical to the controls
used in Lab 4:

	up/down/left/right arrows keys: move primitive in x-y plane.
	, and . keys move primitive along z axis.
	w and s keys move the camera along z axis.
	+ and - keys scale the primitive up and down.
	the \ key changes perspective modes.
	the space bar changes the primitive being rendered.



LIGHT SOURCE:

The location of the light source is defined by 3 constant GLfloats at the
top of Main.cpp. These floats are initially:

	const GLfloat LIGHT_X = 1.0f;
	const GLfloat LIGHT_Y = 0.0f;
	const GLfloat LIGHT_Z = 0.0f;
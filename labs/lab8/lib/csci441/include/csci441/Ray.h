#ifndef RAY_H
#define RAY_H

#include <csci441/vector.h>

class Ray
{
	float originX;
	float originY;
	float originZ;
	float directionX;
	float directionY;
	float directionZ;
	
	public:
		Ray(
			float inOriginX,
			float inOriginY,
			float inOriginZ,
			float inDirectionX,
			float inDirectionY,
			float inDirectionZ) :
			originX(inOriginX),
			originY(inOriginY),
			originZ(inOriginZ),
			directionX(inDirectionX),
			directionY(inDirectionY),
			directionZ(inDirectionZ) {}
		
		Ray() {}
		
		float getOriginX()
		{
			return originX;
		}
		
		float getOriginY()
		{
			return originY;
		}
		
		float getOriginZ()
		{
			return originZ;
		}
		
		float getDirectionX()
		{
			return directionX;
		}
		
		float getDirectionY()
		{
			return directionY;
		}
		
		float getDirectionZ()
		{
			return directionZ;
		}
};

#endif
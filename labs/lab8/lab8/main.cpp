#include <iostream>
#include <string>
#include <sstream>
#include <fstream>
#include <cmath>
#include <vector>

#include <glad/glad.h>
#include <GLFW/glfw3.h>

#include <csci441/shader.h>
#include <csci441/matrix.h>
#include <csci441/matrix3.h>
#include <csci441/vector.h>
#include <csci441/uniform.h>
#include <csci441/Ray.h>
#include <csci441/Sphere.h>

#include "bitmap_image.hpp"


const int WIDTH = 200;
const int HEIGHT = 200;
const bool orthogonal = false;

Vector getColorOfRay(
	Ray ray,
	Sphere* spheres,
	int numberOfSpheres);

int main(void)
{
	Sphere * spheres;
	spheres = new Sphere[3];
	spheres[0] =
		*(new Sphere(
			1.0f,
			0.0f,
			0.0f,
			0.3f,
			255,
			1,
			1));
	spheres[1] =
		*(new Sphere(
			1.0f,
			0.6f,
			0.6f,
			0.5f,
			2,
			176,
			54));
	spheres[2] =
		*(new Sphere(
			1.0f,
			0.0f,
			-0.7f,
			0.2f,
			150,
			3,
			200));
			
	std::vector<Ray> rays;
	rays.reserve(WIDTH * HEIGHT);
	float left = -1.0f;
	float right = 1.0f;
	float top = -1.0f;
	float bottom = 1.0f;
	
	if (orthogonal)
	{
		for (int i = 0; i < WIDTH; i++)
		{
			for (int j = 0; j < HEIGHT; j++)
			{
				float originY = left + (right - left) * ((float)i / (float)WIDTH);
				float originZ = top + (bottom - top) * ((float)j / (float)HEIGHT);
				rays.push_back(Ray(
					0.0f,
					originY,
					originZ,
					0.5f,
					0.0f,
					0.0f));
			}
		}
	}
	else
	{
		for (int i = 0; i < WIDTH; i++)
		{
			for (int j = 0; j < HEIGHT; j++)
			{
				float viewportY = left + (right - left) * ((float)i / (float)WIDTH);
				float viewportZ = top + (bottom - top) * ((float)j / (float)HEIGHT);
				float viewportDistance = 1.25f;
				rays.push_back(Ray(
					0.0f,
					0.0f,
					0.0f,
					viewportDistance,
					viewportY,
					viewportZ));
			}
		}
	}
		
    bitmap_image image(WIDTH, HEIGHT);
	
	for (int i = 0; i < WIDTH; i++)
	{
		for (int j = 0; j < HEIGHT; j++)
		{
			Vector colorVector = getColorOfRay(
				rays.at(WIDTH * i + j),
				spheres,
				3);
				
			int red = colorVector.x();
			int green = colorVector.y();
			int blue = colorVector.z();
			
			if (red == -1)
			{
				red = 14;
			}
			if (green == -1)
			{
				green = 116;
			}
			if (blue == -1)
			{
				blue = 201;
			}
			
			image.set_pixel(
				i,
				j,
				red,
				green,
				blue);
		}
	}
	
    image.save_image("rayTracedImage.bmp");
    std::cout << "Finished" << std::endl;
	
	return 0;
}

Vector getColorOfRay(
	Ray ray,
	Sphere* spheres,
	int numberOfSpheres)
{
	Vector direction = Vector(
		ray.getDirectionX(),
		ray.getDirectionY(),
		ray.getDirectionZ()).normalized();
	
	Vector eye = Vector(
		ray.getOriginX(),
		ray.getOriginY(),
		ray.getOriginZ());
	
	int red = -1;
	int green = -1;
	int blue = -1;
	float lowestT = 999999.9f;
	for (int i = 0; i < numberOfSpheres; i++)
	{
		float radius = spheres[i].getRadius();
		
		Vector center = Vector(
			spheres[i].getCenterX(),
			spheres[i].getCenterY(),
			spheres[i].getCenterZ());
			
		float a = direction * direction;
		float b = direction.scale(2) * (eye - center);
		float c = (eye - center) * (eye - center) - radius * radius;
		
		float determinant = b * b - 4 * a * c;
		
		float bigT, lilT;
		bigT = (-b + pow(b * b - 4 * a * c, 0.5) ) / (2 * a);
		lilT = (-b - pow(b * b - 4 * a * c, 0.5) ) / (2 * a);
		
		if (bigT < lilT &&
			bigT >= 0 &&
			bigT < lowestT)
		{
			lowestT = bigT;
			red = spheres[i].getRed();
			green = spheres[i].getGreen();
			blue = spheres[i].getBlue();
		}
		if (lilT < bigT &&
			lilT >= 0 &&
			lilT < lowestT)
		{
			lowestT = lilT;
			red = spheres[i].getRed();
			green = spheres[i].getGreen();
			blue = spheres[i].getBlue();
		}
	}
		
	return Vector(
		red,
		green,
		blue);
}




























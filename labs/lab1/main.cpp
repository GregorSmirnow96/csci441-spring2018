#include <iostream>
#include "bitmap_image.hpp"
using namespace std;

int main(int argc, char** argv) {
	cout << "Enter 3 points (enter a point as x y r g b):" << endl;
	float x[3];
	float y[3];
	float r[3];
	float g[3];
	float b[3];
	cin >> x[0] >> y[0] >> r[0] >> g[0] >> b[0];
	cin >> x[1] >> y[1] >> r[1] >> g[1] >> b[1];
	cin >> x[2] >> y[2] >> r[2] >> g[2] >> b[2];
	
	for (int i = 0; i < 3; i++)
	{
		r[i] = 255 * r[i];
		g[i] = 255 * g[i];
		b[i] = 255 * b[i];
	}
	
	int width = 640;
	int height = 480;
    bitmap_image image(width, height);
	
	int xMax = -1;
	int yMax = -1;
	int xMin = width + 1;
	int yMin = height + 1;
	
	for (int i = 0; i < 3; i++)
	{
		if (x[i] > xMax)
			xMax = x[i];
		if (y[i] > yMax)
			yMax = y[i];
		if (x[i] < xMin)
			xMin = x[i];
		if (y[i] < yMin)
			yMin = y[i];
	}
	
	bool b1, b2, b3;
	int red, green, blue;
	
	for (int i = xMin; i < xMax; i++)
	{
		for (int j = yMin; j < yMax; j++)
		{
			float fi = (float) i;
			float fj = (float) j;
			float lambdaOne = ((y[1]-y[2])*(fi-x[2])+(x[2]-x[1])*(fj-y[2]))/((y[1]-y[2])*(x[0]-x[2])+(x[2]-x[1])*(y[0]-y[2]));
			float lambdaTwo = ((y[2]-y[0])*(fi-x[2])+(x[0]-x[2])*(fj-y[2]))/((y[1]-y[2])*(x[0]-x[2])+(x[2]-x[1])*(y[0]-y[2]));
			float lambdaThree = 1 - lambdaOne - lambdaTwo;
			b1 = lambdaOne < 0;
			b2 = lambdaTwo < 0;
			b3 = lambdaThree < 0;
			if (b1 == b2 && b2 == b3)
			{
				//red = lambdaOne * r[0] + lambdaTwo * r[1] + lambdaThree * r[2];
				//green = lambdaOne * g[0] + lambdaTwo * g[1] + lambdaThree * g[2];
				//blue = lambdaOne * b[0] + lambdaTwo * b[1] + lambdaThree * b[2];
				float shade = pow(lambdaOne, 2) + pow(lambdaTwo, 2) + pow(lambdaThree, 2);
				image.set_pixel(i, j, shade * 255, shade * 255, shade * 255);
			}
		}
	}
		  
		  /*
      Part 3:
          For each pixel in the triangle, calculate the color based on
          the calculated barycentric coordinates and the 3 provided
          colors. Your colors should have been entered as floating point
          numbers from 0 to 1. The red, green and blue components range
          from 0 to 255. Be sure to make the conversion.
    */

    image.save_image("shaded_triangle.bmp");
    std::cout << "Success" << std::endl;
}

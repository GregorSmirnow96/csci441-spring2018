#include <iostream>
#include <string>
#include <sstream>
#include <fstream>
#include <math.h>

#include <glad/glad.h>
#include <GLFW/glfw3.h>

#include <csci441/shader.h>
using namespace std;

GLfloat* translationMatrix(float x, float y);
GLfloat* scaleMatrix(float x, float y);
GLfloat* rotationMatrix(float degreeOfRotation);
void multiplyMatrices(GLfloat* v1, GLfloat* v2);

void framebufferSizeCallback(GLFWwindow* window, int width, int height) {
    glViewport(0, 0, width, height);
}

void processInput(GLFWwindow *window) {
    if (glfwGetKey(window, GLFW_KEY_ESCAPE) == GLFW_PRESS) {
        glfwSetWindowShouldClose(window, true);
    }
}

int main(void) {
    /* Initialize the library */
    GLFWwindow* window;
    if (!glfwInit()) {
        return -1;
    }

#ifdef __APPLE__
    glfwWindowHint (GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
    glfwWindowHint (GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint (GLFW_CONTEXT_VERSION_MINOR, 3);
    glfwWindowHint (GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
#endif

    /* Create a windowed mode window and its OpenGL context */
    window = glfwCreateWindow(640, 480, "Lab 3", NULL, NULL);
    if (!window) {
        glfwTerminate();
        return -1;
    }

    /* Make the window's context current */
    glfwMakeContextCurrent(window);

    // tell glfw what to do on resize
    glfwSetFramebufferSizeCallback(window, framebufferSizeCallback);

    // init glad
    if (!gladLoadGL()) {
        std::cout << "Failed to initialize OpenGL context" << std::endl;
        glfwTerminate();
        return -1;
    }

    /* init the triangle drawing */
    // define the vertex coordinates of the triangle
    float triangle[] = {
         0.5f,  0.5f, 1.0, 0.0, 0.0,
         0.5f, -0.5f, 1.0, 1.0, 0.0,
        -0.5f,  0.5f, 0.0, 1.0, 0.0,

         0.5f, -0.5f, 1.0, 1.0, 0.0,
        -0.5f, -0.5f, 0.0, 0.0, 1.0,
        -0.5f,  0.5f, 0.0, 1.0, 0.0,
    };

    // create and bind the vertex buffer object and copy the data to the buffer
    GLuint VBO[1];
    glGenBuffers(1, VBO);
    glBindBuffer(GL_ARRAY_BUFFER, VBO[0]);
    glBufferData(GL_ARRAY_BUFFER, sizeof(triangle), triangle, GL_STATIC_DRAW);

    // create and bind the vertex array object and describe data layout
    GLuint VAO[1];
    glGenVertexArrays(1, VAO);
    glBindVertexArray(VAO[0]);

    glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 5*sizeof(float), (void*)(0*sizeof(float)));
    glEnableVertexAttribArray(0);

    glVertexAttribPointer(1, 3, GL_FLOAT, GL_TRUE, 5*sizeof(float), (void*)(2*sizeof(float)));
    glEnableVertexAttribArray(1);

    // create the shaders
	// Shader shader("../vert.glsl", "../frag.glsl");
	Shader shader("vert.glsl", "frag.glsl");
	int movementMatrixId = glGetUniformLocation(shader.id(), "matrix");
	GLfloat* matrix = translationMatrix(0.5,0.0);
	GLfloat* matrix2 = rotationMatrix(40);
	GLfloat* matrix3 = translationMatrix(-0.5, 0.0);
	multiplyMatrices(matrix, matrix2);
	shader.use();
	glUniformMatrix3fv(movementMatrixId, 1, GL_FALSE, matrix);

    /* Loop until the user closes the window */
    while (!glfwWindowShouldClose(window)) {
        // process input
        processInput(window);

        /* Render here */
        glClearColor(0.2f, 0.3f, 0.3f, 1.0f);
        glClear(GL_COLOR_BUFFER_BIT);

        // use the shader
        shader.use();

        /** Part 2 animate and scene by updating the transformation matrix */

        // draw our triangles
        glBindVertexArray(VAO[0]);
        glDrawArrays(GL_TRIANGLES, 0, sizeof(triangle));

        /* Swap front and back * buffers */
        glfwSwapBuffers(window);

        /* Poll for and * process * events */
        glfwPollEvents();
    }

    glfwTerminate();
    return 0;
}

GLfloat* translationMatrix(
	float x,
	float y)
{
	GLfloat *matrix = new float[3];
	matrix[0] = 1.0;
	matrix[1] = 0.0;
	matrix[2] = x;
	matrix[3] = 0.0;
	matrix[4] = 1.0;
	matrix[5] = y;
	matrix[6] = 0.0;
	matrix[7] = 0.0;
	matrix[8] = 1.0;

	return matrix;
}

GLfloat* scaleMatrix(
	float x,
	float y)
{
	GLfloat *matrix = new float[3];
	matrix[0] = x;
	matrix[1] = 0.0;
	matrix[2] = 0.0;
	matrix[3] = 0.0;
	matrix[4] = y;
	matrix[5] = 0.0;
	matrix[6] = 0.0;
	matrix[7] = 0.0;
	matrix[8] = 1.0;

	return matrix;
}

GLfloat* rotationMatrix(float degreeOfRotation)
{
	GLfloat pi = 3.14159265;
	GLfloat s = (float) sin(degreeOfRotation * pi / 180);
	GLfloat c = (float) cos(degreeOfRotation * pi / 180);

	GLfloat *matrix = new float[3];
	matrix[0] = c;
	matrix[1] = -s;
	matrix[2] = 0.0;
	matrix[3] = s;
	matrix[4] = c;
	matrix[5] = 0.0;
	matrix[6] = 0.0;
	matrix[7] = 0.0;
	matrix[8] = 0.0;

	return matrix;
}

void multiplyMatrices(
	GLfloat* v1,
	GLfloat* v2)
{
	/*int matrix[3][3] = { {1,0,0}, {0, 1, 0}, {0, 0, 1} };
	int v[3] = { 1, 2, 3 };
	int x = (matrix[0][0] * v[0]) + (matrix[0][1] * v[1]) + (matrix[0][2] * v[2]);
	int y = matrix[1][0] * v[0] + matrix[1][1] * v[1] + matrix[1][2] * v[2];
	int z = matrix[2][0] * v[0] + matrix[2][1] * v[1] + matrix[2][2] * v[2];
	cout << matrix[0][0] << ", " << matrix[0][1] << ", " << matrix[0][2] << endl;
	cout << v[0] << ", " << v[1] << ", " << v[2] << endl;
	cout << (matrix[0][0] * v[0]) << ", " << (matrix[0][1] * v[1]) << ", " << (matrix[0][2] + v[2]) << endl;
	cout << x << ", " << y << ", " << z << endl;*/

	cout << v1[0] << endl;
}
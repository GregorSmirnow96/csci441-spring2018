#version 330 core
layout (location = 0) in vec2 aPos;
layout (location = 1) in vec3 aColor;

out vec3 myColor;

uniform mat3 matrix;

vec2 transform()
{
	vec3 pos = vec3(aPos, 1.0);
	float x = matrix[0][0] * pos[0] + matrix[0][1] * pos[1] + matrix[0][2] * pos[2];
	float y = matrix[1][0] * pos[0] + matrix[1][1] * pos[1] + matrix[1][2] * pos[2];
	return vec2(x, y);
}

void main()
{
	vec2 newPos = transform();
	gl_Position = vec4(newPos, 0.0, 1.0);
	myColor = aColor;
}
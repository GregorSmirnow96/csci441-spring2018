#include <iostream>
#include <string>
using namespace std;

class Vector3 {
public:
    float x;
    float y;
    float z;

    Vector3(float xx, float yy, float zz) : x(xx), y(yy), z(zz)
    {
	cout << "constructing" << endl;
    }

    Vector3() :x(0), y(0), z(0)
    {
	cout << "constructing" << endl;
    }

    ~Vector3()
    {
	cout << "destructing" << endl;
    }
};

Vector3 add(const Vector3& v, const Vector3& v2)
{
    float x = v.x + v2.x;
    float y = v.y + v2.y;
    float z = v.z + v2.z;
    Vector3 c(x, y, z);
    return c;
}

Vector3 operator+(const Vector3& v, const Vector3& v2)
{
    float x = v.x + v2.x;
    float y = v.y + v2.y;
    float z = v.z + v2.z;
    Vector3 c(x, y, z);
    return c;
}

void operator<<(ostream& outputStream, Vector3 vector)
{
    outputStream << vector.x << ", " << vector.y << ", " << vector.z << endl;
}

int main(int argc, char** argv)
{
    Vector3 vectors[10];
    Vector3* vectorsPointer = vectors;

    for (int i = 0; i < 10; i++)
    {
	vectors[i].y = 5;
	cout << vectors[i];
    }

    Vector3 a(1, 2, 1);
    Vector3 b(2, 4, 2);
    cout << a+b;
}
#include <iostream>
#include <string>
#include <sstream>
#include <fstream>
#include <cmath>

#include <glad/glad.h>
#include <GLFW/glfw3.h>
#include <Camera.h>
#include <Controller.h>
#include <Matrix.h>

#include <csci441/shader.h>

const int SCREEN_WIDTH = 640;
const int SCREEN_HEIGHT = 480;
void updateModelToProjectionMatrix(
	Shader shader,
	Controller controller);
void updateModelMatrix(
	Shader shader,
	Controller controller);
Matrix viewPortMatrix();
Matrix orthographicMatrix(
	GLfloat l,
	GLfloat r,
	GLfloat b,
	GLfloat t,
	GLfloat n,
	GLfloat f);
Matrix perspectiveMatrix(
	GLfloat l,
	GLfloat r,
	GLfloat b,
	GLfloat t,
	GLfloat n,
	GLfloat f);
Matrix translateMatrix(
	float x,
	float y,
	float z);
Matrix scaleMatrix(
	GLfloat x,
	GLfloat y,
	GLfloat z);
Matrix rotationMatrixZ(float theta);
Matrix rotationMatrixY(float theta);
Matrix rotationMatrixX(float theta);
Matrix multiplyMatrices(Matrix m1, Matrix m2);
float* generateCylinderPoints(
	float radius,
	float height,
	int numberOfVertices);
void updateProjectionMode(
	Shader shader,
	int mode);

void framebufferSizeCallback(GLFWwindow* window, int width, int height) {
	glViewport(0, 0, width, height);
}

void processInput(GLFWwindow *window, Shader &shader) {
	if (glfwGetKey(window, GLFW_KEY_ESCAPE) == GLFW_PRESS) {
		glfwSetWindowShouldClose(window, true);
	}
}

void errorCallback(int error, const char* description) {
	fprintf(stderr, "GLFW Error: %s\n", description);
}

int main(void) {
	GLFWwindow* window;

	glfwSetErrorCallback(errorCallback);

	/* Initialize the library */
	if (!glfwInit()) { return -1; }

#ifdef __APPLE__
	glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
#endif

	/* Create a windowed mode window and its OpenGL context */
	window = glfwCreateWindow(SCREEN_WIDTH, SCREEN_HEIGHT, "Lab 4", NULL, NULL);
	if (!window) {
		glfwTerminate();
		return -1;
	}

	/* Make the window's context current */
	glfwMakeContextCurrent(window);

	// tell glfw what to do on resize
	glfwSetFramebufferSizeCallback(window, framebufferSizeCallback);

	// init glad
	if (!gladLoadGL()) {
		std::cerr << "Failed to initialize OpenGL context" << std::endl;
		glfwTerminate();
		return -1;
	}

	/* init the model */
	float cubeVertices[] = {
		-0.5f, -0.5f, -0.5f,  0.0f, 0.0f, 1.0f,
		0.5f, -0.5f, -0.5f,  0.0f, 0.0f, 1.0f,
		0.5f,  0.5f, -0.5f,  0.0f, 0.0f, 1.0f,
		0.5f,  0.5f, -0.5f,  0.0f, 0.0f, 1.0f,
		-0.5f,  0.5f, -0.5f,  0.0f, 0.0f, 1.0f,
		-0.5f, -0.5f, -0.5f,  0.0f, 0.0f, 1.0f,

		-0.5f, -0.5f,  0.5f,  0.0f, 0.0f, 1.0f,
		0.5f, -0.5f,  0.5f,  0.0f, 0.0f, 1.0f,
		0.5f,  0.5f,  0.5f,  0.0f, 0.0f, 1.0f,
		0.5f,  0.5f,  0.5f,  0.0f, 0.0f, 1.0f,
		-0.5f,  0.5f,  0.5f,  0.0f, 0.0f, 1.0f,
		-0.5f, -0.5f,  0.5f,  0.0f, 0.0f, 1.0f,

		-0.5f,  0.5f,  0.5f,  1.0f, 0.0f, 0.0f,
		-0.5f,  0.5f, -0.5f,  1.0f, 0.0f, 0.0f,
		-0.5f, -0.5f, -0.5f,  1.0f, 0.0f, 0.0f,
		-0.5f, -0.5f, -0.5f,  1.0f, 0.0f, 0.0f,
		-0.5f, -0.5f,  0.5f,  1.0f, 0.0f, 0.0f,
		-0.5f,  0.5f,  0.5f,  1.0f, 0.0f, 0.0f,

		0.5f,  0.5f,  0.5f,  1.0f, 0.0f, 0.0f,
		0.5f,  0.5f, -0.5f,  1.0f, 0.0f, 0.0f,
		0.5f, -0.5f, -0.5f,  1.0f, 0.0f, 0.0f,
		0.5f, -0.5f, -0.5f,  1.0f, 0.0f, 0.0f,
		0.5f, -0.5f,  0.5f,  1.0f, 0.0f, 0.0f,
		0.5f,  0.5f,  0.5f,  1.0f, 0.0f, 0.0f,

		-0.5f, -0.5f, -0.5f,  0.0f, 1.0f, 0.0f,
		0.5f, -0.5f, -0.5f,  0.0f, 1.0f, 0.0f,
		0.5f, -0.5f,  0.5f,  0.0f, 1.0f, 0.0f,
		0.5f, -0.5f,  0.5f,  0.0f, 1.0f, 0.0f,
		-0.5f, -0.5f,  0.5f,  0.0f, 1.0f, 0.0f,
		-0.5f, -0.5f, -0.5f,  0.0f, 1.0f, 0.0f,

		-0.5f,  0.5f, -0.5f,  0.0f, 1.0f, 0.0f,
		0.5f,  0.5f, -0.5f,  0.0f, 1.0f, 0.0f,
		0.5f,  0.5f,  0.5f,  0.0f, 1.0f, 0.0f,
		0.5f,  0.5f,  0.5f,  0.0f, 1.0f, 0.0f,
		-0.5f,  0.5f,  0.5f,  0.0f, 1.0f, 0.0f,
		-0.5f,  0.5f, -0.5f,  0.0f, 1.0f, 0.0f
	};
	GLfloat cubeVerticesSize = sizeof(cubeVertices);

	float numberOfVerticesPerCircle = 20;
	float cylinderVerticesSize = (36 * numberOfVerticesPerCircle
		+ 36 * (numberOfVerticesPerCircle - 2)) * sizeof(float);
	float* cylinderVertices =
		generateCylinderPoints(0.2f, 2.0f, numberOfVerticesPerCircle);

	// copy vertex data
	float* vertices = cubeVertices;
	float verticesSize = cubeVerticesSize;
	GLuint VBO;
	glGenBuffers(1, &VBO);
	glBindBuffer(GL_ARRAY_BUFFER, VBO);
	glBufferData(GL_ARRAY_BUFFER, verticesSize, vertices, GL_DYNAMIC_DRAW);

	// describe vertex layout
	GLuint VAO;
	glGenVertexArrays(1, &VAO);
	glBindVertexArray(VAO);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 6 * sizeof(float),
		(void*)0);
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 6 * sizeof(float),
		(void*)(3 * sizeof(float)));
	glEnableVertexAttribArray(1);

	// create the shaders
	Shader shader("vert.glsl", "frag.glsl");

	// setup the textures
	shader.use();

	// and use z-buffering
	glEnable(GL_DEPTH_TEST);

	/* Loop until the user closes the window */
	Controller controller;
	while (!glfwWindowShouldClose(window)) {
		// process input
		processInput(window, shader);

		/* Render here */
		glClearColor(0.2f, 0.3f, 0.3f, 1.0f);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		// activate shader
		shader.use();
		
		// create transformation matrix and pass it to shader
		controller.recieveUserInput();
		if (controller.primitiveIndex == 0)
		{
			vertices = cylinderVertices;
			verticesSize = cylinderVerticesSize;
			GLuint VBOCylinder;
			glGenBuffers(3, &VBOCylinder);
			glBindBuffer(GL_ARRAY_BUFFER, VBOCylinder);
			glBufferData(GL_ARRAY_BUFFER, verticesSize, vertices, GL_DYNAMIC_DRAW);

			GLuint VAOCylinder;
			glGenVertexArrays(3, &VAOCylinder);
			glBindVertexArray(VAOCylinder);
			glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 6 * sizeof(float),
				(void*)0);
			glEnableVertexAttribArray(0);
			glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 6 * sizeof(float),
				(void*)(3 * sizeof(float)));
			glEnableVertexAttribArray(1);

			VAO = VAOCylinder;
		}
		if (controller.primitiveIndex == 1)
		{
			vertices = cubeVertices;
			verticesSize = cubeVerticesSize;
			GLuint VBOCube;
			glGenBuffers(4, &VBOCube);
			glBindBuffer(GL_ARRAY_BUFFER, VBOCube);
			glBufferData(GL_ARRAY_BUFFER, verticesSize, vertices, GL_DYNAMIC_DRAW);

			GLuint VAOCube;
			glGenVertexArrays(4, &VAOCube);
			glBindVertexArray(VAOCube);
			glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 6 * sizeof(float),
				(void*)0);
			glEnableVertexAttribArray(0);
			glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 6 * sizeof(float),
				(void*)(3 * sizeof(float)));
			glEnableVertexAttribArray(1);

			VAO = VAOCube;
		}
		updateProjectionMode(shader, controller.projectionMode);
		updateModelMatrix(shader, controller);
		updateModelToProjectionMatrix(shader, controller);

		// render the cube
		glBindVertexArray(VAO);
		glDrawArrays(GL_TRIANGLES, 0, verticesSize);

		/* Swap front and back and poll for io events */
		glfwSwapBuffers(window);
		glfwPollEvents();

		Sleep(17);
	}

	glfwTerminate();
	return 0;
}

void updateProjectionMode(
	Shader shader,
	int mode)
{
	//mode = 0 => orthographic
	//mode = 1 => perspective
	GLint shaderVariableIndex =
		glGetUniformLocation(
			shader.id(),
			"projectionMode");
	glUniform1i(
		shaderVariableIndex,
		mode);
}


void updateModelToProjectionMatrix(
	Shader shader,
	Controller controller)
{
	Matrix vpMatrix = viewPortMatrix();
	Matrix orthMatrix = orthographicMatrix(-1.0f, 1.0f, -1.0f, 1.0f, 1.0f, 50.0f);
	Matrix perspMatrix = perspectiveMatrix(-1.0f, 1.0f, -1.0f, 1.0f, 1.0f, 50.0f);
	Matrix eyeSpaceMatrix = translateMatrix(0.0f, 0.0f, controller.cameraLocationZ);

	Matrix* projection;
	if (controller.projectionMode == 0) {
		Matrix m = orthMatrix;
		projection = &m;
	}
	else {
		Matrix m = perspMatrix;
		projection = &m;
	}
	Matrix n = *projection * eyeSpaceMatrix;

	n.passToShader(
		shader,
		"cameraTransformMatrix");
}

void updateModelMatrix(
	Shader shader,
	Controller controller)
{
	Matrix translateM = translateMatrix(
		controller.modelTranslateX,
		controller.modelTranslateY,
		controller.modelTranslateZ);
	Matrix scaleM = scaleMatrix(
		controller.modelScale,
		controller.modelScale,
		controller.modelScale);
	Matrix x = rotationMatrixX(controller.xRotationDegree);
	Matrix y = rotationMatrixY(controller.yRotationDegree);
	Matrix z = rotationMatrixZ(controller.zRotationDegree);

	Matrix* m = &(y * x);
	m = &(z * *m);
	m = &(scaleM * *m);
	m = &(translateM * *m);

	m->passToShader(
		shader,
		"modelMatrix");
}

Matrix viewPortMatrix()
{
	GLfloat xMultiply = SCREEN_WIDTH / 2;
	GLfloat xAdd = SCREEN_WIDTH / 2;
	GLfloat yMultiply = SCREEN_HEIGHT / 2;
	GLfloat yAdd = SCREEN_HEIGHT / 2;
	GLfloat matrixElements[16] = {
		 xMultiply , 0.0f, 0.0f,  xAdd,
		0.0f,  yMultiply , 0.0f,  yAdd,
		0.0f, 0.0f, 1.0f, 0.0f,
		0.0f, 0.0f, 0.0f, 1.0f
	};

	Matrix matrix(4, 4);
	matrix.populate(matrixElements);

	return matrix;
}

Matrix orthographicMatrix(
	GLfloat l,
	GLfloat r,
	GLfloat b,
	GLfloat t,
	GLfloat n,
	GLfloat f)
{
	GLfloat x1 = 2 / (r - l);
	GLfloat x2 = -((r + l) / (r - l));
	GLfloat y1 = 2 / (t - b);
	GLfloat y2 = -((t + b) / (t - b));
	GLfloat z1 = -2 / (f - n);
	GLfloat z2 = -((n + f) / (f - n));
	GLfloat matrixElements[16] = {
		 x1 , 0.0f, 0.0f,  x2,
		0.0f,  y1 , 0.0f,  y2,
		0.0f, 0.0f,  z1 ,  z2,
		0.0f, 0.0f, 0.0f, 1.0f
	};

	Matrix matrix(4, 4);
	matrix.populate(matrixElements);

	return matrix;
}

Matrix perspectiveMatrix(
	GLfloat l,
	GLfloat r,
	GLfloat b,
	GLfloat t,
	GLfloat n,
	GLfloat f)
{
	GLfloat x1 = (2 * abs(n)) / (r - l);
	GLfloat y1 = (2 * abs(n)) / (t - b);
	GLfloat z1 = (abs(n) + abs(f)) / (abs(n) - abs(f));
	GLfloat x3 = (r + l) / (r - l);
	GLfloat y3 = (t + b) / (t - b);
	GLfloat z4 = (2 * (abs(n) * abs(f))) / (abs(n) - abs(f));

	GLfloat elements[16] = {
		x1, 0.0f, x3, 0.0f,
		0.0f, y1, y3, 0.0f,
		0.0f, 0.0f, z1, z4,
		0.0f, 0.0f, -1.0f, 0.0f
	};

	Matrix matrix(4, 4);
	matrix.populate(elements);
	return matrix;
}

Matrix translateMatrix(
	float x,
	float y,
	float z)
{
	GLfloat matrixElements[16] = {
		1.0f, 0.0f, 0.0f,  x  ,
		0.0f, 1.0f, 0.0f,  y  ,
		0.0f, 0.0f, 1.0f,  z  ,
		0.0f, 0.0f, 0.0f, 1.0f
	};

	Matrix matrix(4, 4);
	matrix.populate(matrixElements);

	return matrix;
}

Matrix scaleMatrix(
	GLfloat scaleX,
	GLfloat scaleY,
	GLfloat scaleZ)
{
	GLfloat matrixElements[16] = {
		scaleX, 0.0f, 0.0f, 0.0f,
		0.0f, scaleY, 0.0f, 0.0f,
		0.0f, 0.0f, scaleZ, 0.0f,
		0.0f, 0.0f, 0.0f, 1.0f
	};

	Matrix matrix(4, 4);
	matrix.populate(matrixElements);

	return matrix;
}

Matrix rotationMatrixX(float theta)
{
	GLfloat pi = 3.14159265f;
	float c = cos(theta * pi / 180);
	float s = sin(theta * pi / 180);

	GLfloat elements[16] = {
		  c , 0.0f,  -s , 0.0f,
		0.0f, 1.0f, 0.0f, 0.0f,
		  s , 0.0f,   c , 0.0f,
		0.0f, 0.0f, 0.0f, 1.0f
	};
	Matrix matrix(4, 4);
	matrix.populate(elements);

	return matrix;
}

Matrix rotationMatrixY(float theta)
{
	GLfloat pi = 3.14159265f;
	float c = cos(theta * pi / 180);
	float s = sin(theta * pi / 180);

	GLfloat elements[16] = {
		1.0f, 0.0f, 0.0f, 0.0f,
		0.0f,  c  ,  -s , 0.0f,
		0.0f,  s  ,   c , 0.0f,
		0.0f, 0.0f, 0.0f, 1.0f
	};
	Matrix matrix(4, 4);
	matrix.populate(elements);

	return matrix;
}

Matrix rotationMatrixZ(float theta)
{
	GLfloat pi = 3.14159265f;
	float c = cos(theta * pi / 180);
	float s = sin(theta * pi / 180);

	GLfloat elements[16] = {
		  c ,  -s , 0.0f, 0.0f,
		  s ,   c , 0.0f, 0.0f,
		0.0f, 0.0f, 1.0f, 0.0f,
		0.0f, 0.0f, 0.0f, 1.0f
	};
	Matrix matrix(4, 4);
	matrix.populate(elements);

	return matrix;
}

Matrix multiplyMatrices(
	Matrix m1,
	Matrix m2)
{
	int sizeOfNewMatrix = m1.height * m2.width;
	Matrix matrix(m1.height, m2.width);
	GLfloat* elements = new GLfloat[sizeOfNewMatrix];
	for (int i = 0; i < m1.height; i++)
	{
		for (int j = 0; j < m2.width; j++)
		{
			GLfloat matrixEntry = 0;
			for (int k = 0; k < m1.width; k++)
			{
				GLfloat elementOne = m1.getElementAt(i, k);
				GLfloat elementTwo = m2.getElementAt(k, j);
				matrixEntry += elementOne * elementTwo;
			}
			elements[j + (i * m1.height)] = matrixEntry;
		}
	}

	matrix.populate(elements);
	delete elements;

	return matrix;
}

float* generateCylinderPoints(
	float radius,
	float height,
	int numberOfVerticesPerCircle)
{
	Matrix topOrderedPairs(numberOfVerticesPerCircle, 3);
	Matrix bottomOrderedPairs(numberOfVerticesPerCircle, 3);
	float PI = 3.14159f;
	float thetaIncrement = 360.0f / numberOfVerticesPerCircle;
	float theta;

	for (int i = 0; i < numberOfVerticesPerCircle; i++)
	{
		theta = 0.0f + i * thetaIncrement;
		float xCoord = cos(theta * PI / 180.0f);
		float yCoord = sin(theta * PI / 180.0f);
		float zCoord = height / 2.0f;
		topOrderedPairs.setElementAt(i, 0, xCoord);
		topOrderedPairs.setElementAt(i, 1, yCoord);
		topOrderedPairs.setElementAt(i, 2, zCoord);
		bottomOrderedPairs.setElementAt(i, 0, xCoord);
		bottomOrderedPairs.setElementAt(i, 1, yCoord);
		bottomOrderedPairs.setElementAt(i, 2, -zCoord);
	}

	int numberOfPointsPerFace = 36;
	float* vertices = new float[numberOfPointsPerFace * numberOfVerticesPerCircle
		+ 36 * (numberOfVerticesPerCircle - 2)];

	cout << numberOfPointsPerFace * numberOfVerticesPerCircle
		+ 36 * (numberOfVerticesPerCircle - 2) << endl;

	for (int i = 0; i < numberOfVerticesPerCircle; i++)
	{
		vertices[0 + i * numberOfPointsPerFace] = topOrderedPairs.getElementAt(i, 0);
		vertices[1 + i * numberOfPointsPerFace] = topOrderedPairs.getElementAt(i, 1);
		vertices[2 + i * numberOfPointsPerFace] = topOrderedPairs.getElementAt(i, 2);
		vertices[6 + i * numberOfPointsPerFace] = topOrderedPairs.getElementAt((i + 1) % numberOfVerticesPerCircle, 0);
		vertices[7 + i * numberOfPointsPerFace] = topOrderedPairs.getElementAt((i + 1) % numberOfVerticesPerCircle, 1);
		vertices[8 + i * numberOfPointsPerFace] = topOrderedPairs.getElementAt((i + 1) % numberOfVerticesPerCircle, 2);
		vertices[12 + i * numberOfPointsPerFace] = bottomOrderedPairs.getElementAt((i + 1) % numberOfVerticesPerCircle, 0);
		vertices[13 + i * numberOfPointsPerFace] = bottomOrderedPairs.getElementAt((i + 1) % numberOfVerticesPerCircle, 1);
		vertices[14 + i * numberOfPointsPerFace] = bottomOrderedPairs.getElementAt((i + 1) % numberOfVerticesPerCircle, 2);
		vertices[3 + i * numberOfPointsPerFace] = 1;
		vertices[4 + i * numberOfPointsPerFace] = 0;
		vertices[5 + i * numberOfPointsPerFace] = 0;
		vertices[9 + i * numberOfPointsPerFace] = 1;
		vertices[10 + i * numberOfPointsPerFace] = 0;
		vertices[11 + i * numberOfPointsPerFace] = 0;
		vertices[15 + i * numberOfPointsPerFace] = 1;
		vertices[16 + i * numberOfPointsPerFace] = 0;
		vertices[17 + i * numberOfPointsPerFace] = 0;

		vertices[18 + i * numberOfPointsPerFace] = topOrderedPairs.getElementAt(i, 0);
		vertices[19 + i * numberOfPointsPerFace] = topOrderedPairs.getElementAt(i, 1);
		vertices[20 + i * numberOfPointsPerFace] = topOrderedPairs.getElementAt(i, 2);
		vertices[24 + i * numberOfPointsPerFace] = bottomOrderedPairs.getElementAt(i, 0);
		vertices[25 + i * numberOfPointsPerFace] = bottomOrderedPairs.getElementAt(i, 1);
		vertices[26 + i * numberOfPointsPerFace] = bottomOrderedPairs.getElementAt(i, 2);
		vertices[30 + i * numberOfPointsPerFace] = bottomOrderedPairs.getElementAt((i + 1) % numberOfVerticesPerCircle, 0);
		vertices[31 + i * numberOfPointsPerFace] = bottomOrderedPairs.getElementAt((i + 1) % numberOfVerticesPerCircle, 1);
		vertices[32 + i * numberOfPointsPerFace] = bottomOrderedPairs.getElementAt((i + 1) % numberOfVerticesPerCircle, 2);
		vertices[21 + i * numberOfPointsPerFace] = 1;
		vertices[22 + i * numberOfPointsPerFace] = 0;
		vertices[23 + i * numberOfPointsPerFace] = 0;
		vertices[27 + i * numberOfPointsPerFace] = 1;
		vertices[28 + i * numberOfPointsPerFace] = 0;
		vertices[29 + i * numberOfPointsPerFace] = 0;
		vertices[33 + i * numberOfPointsPerFace] = 1;
		vertices[34 + i * numberOfPointsPerFace] = 0;
		vertices[35 + i * numberOfPointsPerFace] = 0;
	}

	//vertex to brnch off from

	for (int i = 0; i < numberOfVerticesPerCircle - 2; i++) {
		//top
		cout << 0 + (i + numberOfVerticesPerCircle) * numberOfPointsPerFace << endl;
		vertices[0 + (i + numberOfVerticesPerCircle) * numberOfPointsPerFace] = topOrderedPairs.getElementAt(0, 0);
		vertices[1 + (i + numberOfVerticesPerCircle) * numberOfPointsPerFace] = topOrderedPairs.getElementAt(0, 1);
		vertices[2 + (i + numberOfVerticesPerCircle) * numberOfPointsPerFace] = topOrderedPairs.getElementAt(0, 2);
		vertices[3 + (i + numberOfVerticesPerCircle) * numberOfPointsPerFace] = 0;
		vertices[4 + (i + numberOfVerticesPerCircle) * numberOfPointsPerFace] = 1;
		vertices[5 + (i + numberOfVerticesPerCircle) * numberOfPointsPerFace] = 0;

		vertices[6 + (i + numberOfVerticesPerCircle) * numberOfPointsPerFace] = topOrderedPairs.getElementAt(i+1, 0);
		vertices[7 + (i + numberOfVerticesPerCircle) * numberOfPointsPerFace] = topOrderedPairs.getElementAt(i+1, 1);
		vertices[8 + (i + numberOfVerticesPerCircle) * numberOfPointsPerFace] = topOrderedPairs.getElementAt(i+1, 2);
		vertices[9 + (i + numberOfVerticesPerCircle) * numberOfPointsPerFace] = 0;
		vertices[10 + (i + numberOfVerticesPerCircle) * numberOfPointsPerFace] = 1;
		vertices[11 + (i + numberOfVerticesPerCircle) * numberOfPointsPerFace] = 0;

		vertices[12 + (i + numberOfVerticesPerCircle) * numberOfPointsPerFace] = topOrderedPairs.getElementAt(i+2, 0);
		vertices[13 + (i + numberOfVerticesPerCircle) * numberOfPointsPerFace] = topOrderedPairs.getElementAt(i+2, 1);
		vertices[14 + (i + numberOfVerticesPerCircle) * numberOfPointsPerFace] = topOrderedPairs.getElementAt(i+2, 2);
		vertices[15 + (i + numberOfVerticesPerCircle) * numberOfPointsPerFace] = 0;
		vertices[16 + (i + numberOfVerticesPerCircle) * numberOfPointsPerFace] = 1;
		vertices[17 + (i + numberOfVerticesPerCircle) * numberOfPointsPerFace] = 0;

		//bottom
		vertices[18 + (i + numberOfVerticesPerCircle) * numberOfPointsPerFace] = bottomOrderedPairs.getElementAt(0, 0);
		vertices[19 + (i + numberOfVerticesPerCircle) * numberOfPointsPerFace] = bottomOrderedPairs.getElementAt(0, 1);
		vertices[20 + (i + numberOfVerticesPerCircle) * numberOfPointsPerFace] = bottomOrderedPairs.getElementAt(0, 2);
		vertices[21 + (i + numberOfVerticesPerCircle) * numberOfPointsPerFace] = 0;
		vertices[22 + (i + numberOfVerticesPerCircle) * numberOfPointsPerFace] = 0;
		vertices[23 + (i + numberOfVerticesPerCircle) * numberOfPointsPerFace] = 1;

		vertices[24 + (i + numberOfVerticesPerCircle) * numberOfPointsPerFace] = bottomOrderedPairs.getElementAt(i+1, 0);
		vertices[25 + (i + numberOfVerticesPerCircle) * numberOfPointsPerFace] = bottomOrderedPairs.getElementAt(i+1, 1);
		vertices[26 + (i + numberOfVerticesPerCircle) * numberOfPointsPerFace] = bottomOrderedPairs.getElementAt(i+1, 2);
		vertices[27 + (i + numberOfVerticesPerCircle) * numberOfPointsPerFace] = 0;
		vertices[28 + (i + numberOfVerticesPerCircle) * numberOfPointsPerFace] = 0;
		vertices[29 + (i + numberOfVerticesPerCircle) * numberOfPointsPerFace] = 1;

		vertices[30 + (i + numberOfVerticesPerCircle) * numberOfPointsPerFace] = bottomOrderedPairs.getElementAt(i+2, 0);
		vertices[31 + (i + numberOfVerticesPerCircle) * numberOfPointsPerFace] = bottomOrderedPairs.getElementAt(i+2, 1);
		vertices[32 + (i + numberOfVerticesPerCircle) * numberOfPointsPerFace] = bottomOrderedPairs.getElementAt(i+2, 2);
		vertices[33 + (i + numberOfVerticesPerCircle) * numberOfPointsPerFace] = 0;
		vertices[34 + (i + numberOfVerticesPerCircle) * numberOfPointsPerFace] = 0;
		vertices[35 + (i + numberOfVerticesPerCircle) * numberOfPointsPerFace] = 1;
	}

	return vertices;
}
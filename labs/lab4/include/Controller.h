#ifndef CONTROLLER_H
#define CONTROLLER_H

#include <Camera.h>

class Controller
{
	public:
		GLfloat xRotationDegree = 0.0f;
		GLfloat yRotationDegree = 0.0f;
		GLfloat zRotationDegree = 0.0f;
		GLfloat modelTranslateX = 0.0f;
		GLfloat modelTranslateY = 0.0f;
		GLfloat modelTranslateZ = 0.0f;
		GLfloat modelScale = 1.0f;
		GLfloat cameraLocationZ = -20.0f;
		GLfloat cameraLocationY = 0.0f;
		GLfloat cameraLocationX = 0.0f;
		int primitiveIndex = 0; // 0 - cylinder   1 - cube
		int projectionMode = 0;
		Camera* camera;

		Controller()
		{
			camera = new Camera(0.0f, 0.0f, 0.0f);
		}

		~Controller() {}

		void recieveUserInput()
		{
			int up = GetAsyncKeyState(VK_UP);
			int down = GetAsyncKeyState(VK_DOWN);
			int left = GetAsyncKeyState(VK_LEFT);
			int right = GetAsyncKeyState(VK_RIGHT);
			int in = GetAsyncKeyState(VK_OEM_COMMA);
			int out = GetAsyncKeyState(VK_OEM_PERIOD);
			int xRotatePos = GetAsyncKeyState(0x55);
			int xRotateNeg = GetAsyncKeyState(0x49);
			int yRotatePos = GetAsyncKeyState(0x4F);
			int yRotateNeg = GetAsyncKeyState(0x50);
			int zRotatePos = GetAsyncKeyState(VK_OEM_4);
			int zRotateNeg = GetAsyncKeyState(VK_OEM_6);
			int scaleUp = GetAsyncKeyState(VK_OEM_PLUS);
			int scaleDown = GetAsyncKeyState(VK_OEM_MINUS);
			int moveForward = GetAsyncKeyState(0x57);
			int moveBackward = GetAsyncKeyState(0x53);
			int primitiveChange = GetAsyncKeyState(VK_SPACE);
			int modeChange = GetAsyncKeyState(VK_OEM_5);

			if (primitiveChange)
			{
				primitiveIndex = (primitiveIndex + 1) % 2;
				cout << primitiveIndex << endl;
				Sleep(500);
			}
			if (modeChange)
			{
				projectionMode = (projectionMode + 1) % 2;
				std::cout << "Mode changed to: " << projectionMode << std::endl;
				Sleep(500);
			}
			if (right)
			{
				modelTranslateX += 0.01f;
			}
			if (left)
			{
				modelTranslateX -= 0.01f;
			}
			if (up)
			{
				modelTranslateY += 0.01f;
			}
			if (down)
			{
				modelTranslateY -= 0.01f;
			}
			if (in)
			{
				modelTranslateZ += 0.05f;
			}
			if (out)
			{
				modelTranslateZ -= 0.05f;
			}
			if (xRotatePos)
			{
				xRotationDegree += 1.0f;
			}
			if (xRotateNeg)		   
			{					   
				xRotationDegree -= 1.0f;
			}					   
			if (yRotatePos)		   
			{					   
				yRotationDegree += 1.0f;
			}					   
			if (yRotateNeg)		   
			{					   
				yRotationDegree -= 1.0f;
			}					   
			if (zRotatePos)
			{
				zRotationDegree += 1.0f;
			}
			if (zRotateNeg)		   
			{
				zRotationDegree -= 1.0f;
			}
			if (scaleUp)
			{
				modelScale += 0.02f;
			}
			if (scaleDown)
			{
				modelScale -= 0.02f;
			}
			if (moveForward && cameraLocationZ < 20.0f)
			{
				cameraLocationZ += 0.05f;
			}
			if (moveBackward && cameraLocationZ > -20.0f)
			{
				cameraLocationZ -= 0.05f;
			}
		}
};

#endif
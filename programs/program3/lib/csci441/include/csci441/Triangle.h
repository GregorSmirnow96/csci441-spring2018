#ifndef TRIANGLE_H
#define TRIANGLE_H

#include <csci441/Vertex.h>

class Triangle
{
	std::vector<float> vertex1;
	std::vector<float> vertex2;
	std::vector<float> vertex3;
	bool isMirror;
	int index;
	
	public:
		Triangle() {}
		
		Triangle(
			std::vector<float> inVertex1,
			std::vector<float> inVertex2,
			std::vector<float> inVertex3) :
			vertex1(inVertex1),
			vertex2(inVertex2),
			vertex3(inVertex3),
			isMirror(false)
		{
			generateNormal();
		}
		
		std::vector<float> getVertex1()
		{
			return vertex1;
		}
		
		std::vector<float> getVertex2()
		{
			return vertex2;
		}
		
		std::vector<float> getVertex3()
		{
			return vertex3;
		}
		
		void setAsMirror()
		{
			isMirror = true;
		}
		
		bool isAMirror()
		{
			return isMirror;
		}
		
		void setIndex(int inIndex)
		{
			index = inIndex;
		}
		
		int getIndex()
		{
			return index;
		}
		
		void generateNormal()
		{
			float x1 = vertex1.at(0);
			float y1 = vertex1.at(1);
			float z1 = vertex1.at(2);
			float x2 = vertex2.at(0);
			float y2 = vertex2.at(1);
			float z2 = vertex2.at(2);
			float x3 = vertex3.at(0);
			float y3 = vertex3.at(1);
			float z3 = vertex3.at(2);
			
			float ux = x3 - x1;
			float uy = y3 - y1;
			float uz = z3 - z1;
			float vx = x2 - x1;
			float vy = y2 - y1;
			float vz = z2 - z1;
			
			float crossX = uy * vz - uz * vy;
			float crossY = uz * vx - ux * vz;
			float crossZ = ux * vy - uy * vx;
			
			vertex1.push_back(crossX);
			vertex1.push_back(crossY);
			vertex1.push_back(crossZ);
			vertex2.push_back(crossX);
			vertex2.push_back(crossY);
			vertex2.push_back(crossZ);
			vertex3.push_back(crossX);
			vertex3.push_back(crossY);
			vertex3.push_back(crossZ);
		}
		
		void setNormal1(Vector inVector)
		{
			vertex1.at(6) = inVector.x();
			vertex1.at(7) = inVector.y();
			vertex1.at(8) = inVector.z();
		}
		
		void setNormal2(Vector inVector)
		{
			vertex2.at(6) = inVector.x();
			vertex2.at(7) = inVector.y();
			vertex2.at(8) = inVector.z();
		}
		
		void setNormal3(Vector inVector)
		{
			vertex3.at(6) = inVector.x();
			vertex3.at(7) = inVector.y();
			vertex3.at(8) = inVector.z();
		}
};

#endif
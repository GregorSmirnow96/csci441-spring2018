#ifndef FACTORY_H
#define FACTORY_H

#include <vector>
#include <csci441/vector.h>

class Factory
{
	public:
		Triangle* generateTriangles(int NUMBER_OF_TRIAGLES)
		{
			Triangle* triangles;
			triangles = new Triangle[NUMBER_OF_TRIAGLES];
			
			std::vector<float> vertex1;
			vertex1.reserve(9);
			vertex1.push_back(2.0f);
			vertex1.push_back(1.0f);
			vertex1.push_back(-1.0f);
			vertex1.push_back(255);
			vertex1.push_back(0);
			vertex1.push_back(0);
			std::vector<float> vertex2;
			vertex2.reserve(9);
			vertex2.push_back(2.0f);
			vertex2.push_back(-1.0f);
			vertex2.push_back(1.0f);
			vertex2.push_back(255);
			vertex2.push_back(255);
			vertex2.push_back(0);
			std::vector<float> vertex3;
			vertex3.reserve(9);
			vertex3.push_back(2.0f);
			vertex3.push_back(-1.0f);
			vertex3.push_back(-1.0f);
			vertex3.push_back(255);
			vertex3.push_back(0);
			vertex3.push_back(0);
			
			triangles[0] =
				*(new Triangle(
					vertex1,
					vertex2,
					vertex3));
			triangles[0].setIndex(0);
			
			std::vector<float> vertex4;
			vertex4.reserve(9);
			vertex4.push_back(2.0f);
			vertex4.push_back(1.0f);
			vertex4.push_back(-1.0f);
			vertex4.push_back(255);
			vertex4.push_back(0);
			vertex4.push_back(0);
			std::vector<float> vertex5;
			vertex5.reserve(9);
			vertex5.push_back(2.0f);
			vertex5.push_back(1.0f);
			vertex5.push_back(1.0f);
			vertex5.push_back(255);
			vertex5.push_back(0);
			vertex5.push_back(0);
			std::vector<float> vertex6;
			vertex6.reserve(9);
			vertex6.push_back(2.0f);
			vertex6.push_back(-1.0f);
			vertex6.push_back(1.0f);
			vertex6.push_back(255);
			vertex6.push_back(0);
			vertex6.push_back(0);
			
			triangles[1] =
				*(new Triangle(
					vertex4,
					vertex5,
					vertex6));
			triangles[1].setIndex(1);
			
			std::vector<float> vertex7;
			vertex7.reserve(9);
			vertex7.push_back(-1.0f);
			vertex7.push_back(0.8f);
			vertex7.push_back(-0.8f);
			vertex7.push_back(255);
			vertex7.push_back(0);
			vertex7.push_back(0);
			std::vector<float> vertex8;
			vertex8.reserve(9);
			vertex8.push_back(-1.0f);
			vertex8.push_back(-0.8f);
			vertex8.push_back(0.8f);
			vertex8.push_back(255);
			vertex8.push_back(0);
			vertex8.push_back(0);
			std::vector<float> vertex9;
			vertex9.reserve(9);
			vertex9.push_back(-1.0f);
			vertex9.push_back(-0.6f);
			vertex9.push_back(-0.6f);
			vertex9.push_back(255);
			vertex9.push_back(0);
			vertex9.push_back(0);
			
			triangles[2] =
				*(new Triangle(
					vertex7,
					vertex8,
					vertex9));
			triangles[2].setAsMirror();
			triangles[2].setIndex(2);
			
			std::vector<float> vertex10;
			vertex10.reserve(9);
			vertex10.push_back(-1.0f);
			vertex10.push_back(0.8f);
			vertex10.push_back(-0.8f);
			vertex10.push_back(255);
			vertex10.push_back(0);
			vertex10.push_back(0);
			std::vector<float> vertex11;
			vertex11.reserve(9);
			vertex11.push_back(-1.0f);
			vertex11.push_back(0.8f);
			vertex11.push_back(0.8f);
			vertex11.push_back(255);
			vertex11.push_back(0);
			vertex11.push_back(0);
			std::vector<float> vertex12;
			vertex12.reserve(9);
			vertex12.push_back(-1.0f);
			vertex12.push_back(-0.8f);
			vertex12.push_back(0.8f);
			vertex12.push_back(255);
			vertex12.push_back(0);
			vertex12.push_back(0);
			
			triangles[3] =
				*(new Triangle(
					vertex10,
					vertex11,
					vertex12));
			triangles[3].setAsMirror();
			triangles[3].setIndex(2);
			
			Vector tlNormal = Vector(
				-1.0f,
				-1.0f,
				1.0f);
			Vector brNormal = Vector(
				-1.0f,
				1.0f,
				-1.0f);
			Vector blNormal = Vector(
				-1.0f,
				-1.0f,
				-1.0f);
			Vector trNormal = Vector(
				-1.0f,
				1.0f,
				1.0f);
				
			triangles[0].setNormal1(blNormal);
			triangles[0].setNormal2(tlNormal);
			triangles[0].setNormal3(trNormal);
			triangles[1].setNormal1(brNormal);
			triangles[1].setNormal2(blNormal);
			triangles[1].setNormal3(trNormal);
			
			return triangles;
		}

		std::vector<Ray> generateRays(
			int WIDTH,
			int HEIGHT,
			bool orthogonal)
		{
			std::vector<Ray> rays;
			rays.reserve(WIDTH * HEIGHT);
			
			float left = -1.0f;
			float right = 1.0f;
			float top = -1.0f;
			float bottom = 1.0f;
			
			if (orthogonal)
			{
				for (int i = 0; i < WIDTH; i++)
				{
					for (int j = 0; j < HEIGHT; j++)
					{
						float originY = left + (right - left) * ((float)i / (float)WIDTH);
						float originZ = top + (bottom - top) * ((float)j / (float)HEIGHT);
						Ray ray = Ray(
							0.0f,
							originY,
							originZ,
							-0.5f,
							0.0f,
							0.0f);
						rays.push_back(ray);
					}
				}
			}
			else
			{
				for (int i = 0; i < WIDTH; i++)
				{
					for (int j = 0; j < HEIGHT; j++)
					{
						float viewportY = left + (right - left) * ((float)i / (float)WIDTH);
						float viewportZ = top + (bottom - top) * ((float)j / (float)HEIGHT);
						float viewportDistance = 1.0f;
						rays.push_back(Ray(
							0.0f,
							0.0f,
							0.0f,
							viewportDistance,
							viewportY,
							viewportZ));
					}
				}
			}
			
			return rays;
		}
};

#endif
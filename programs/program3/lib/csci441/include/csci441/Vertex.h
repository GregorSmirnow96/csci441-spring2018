#ifndef _CSCI441_SHAPE_H_
#define _CSCI441_SHAPE_H_

#include <vector>

class Vertex
{
	std::vector<float> position;
	std::vector<float> color;
	std::vector<float> normal;
	
	public:
		Vertex(
			std::vector<float> inPosition,
			std::vector<float> inColor) :
			position(inPosition),
			color(inColor) {}
		
		std::vector<float> getPosition()
		{
			return position;
		}
		
		std::vector<float> getColor()
		{
			return color;
		}
		
		std::vector<float> getNormal()
		{
			return normal;
		}
		
		void setNormal(std::vector<float> inNormal)
		{
			normal = inNormal;
		}
};

#endif
#include <iostream>
#include <string>
#include <sstream>
#include <fstream>
#include <cmath>
#include <vector>

#include <glad/glad.h>
#include <GLFW/glfw3.h>

#include <csci441/shader.h>
#include <csci441/matrix.h>
#include <csci441/matrix3.h>
#include <csci441/vector.h>
#include <csci441/uniform.h>
#include <csci441/Ray.h>
#include <csci441/Triangle.h>
#include <csci441/Factory.h>

#include <SFML/Graphics.hpp>

#include "bitmap_image.hpp"


const int WIDTH = 400;
const int HEIGHT = 400;
const int NUMBER_OF_TRIAGLES = 4;
const bool orthogonal = true;
const int backgroundRed = 100;
const int backgroundGreen = 190;
const int backgroundBlue = 240;
const float lightSourceX = -5.0f;
const float lightSourceY = 0.0f;
const float lightSourceZ = 0.0f;
const float PHONG_POW = 1.7f;
int previousTriangleIndex = -1;
int rayBounces = 0;


Vector getColorOfRay(
	Ray ray,
	Triangle* triangles,
	int numberOfTriangles);
	
Vector cramersRule(
	Matrix3 matrix,
	Vector vector);
	
Ray calculateReflectedRay(
	Ray ray,
	Triangle triangle,
	Vector intersection);
	
float determinate3x3(Matrix3 matrix);

bitmap_image generateImage(
	Triangle* triangles,
	std::vector<Ray> rays);
	
float calculateLightFactor(
	Triangle triangles,
	float u,
	float v,
	float w);

int main(void)
{
	Factory factory = Factory();
	Triangle * triangles =
		factory.generateTriangles(NUMBER_OF_TRIAGLES);
	std::vector<Ray> rays = factory.generateRays(
		WIDTH,
		HEIGHT,
		orthogonal);
	
    bitmap_image image =
		generateImage(
			triangles,
			rays);
    image.save_image("rayTracedImage.bmp");
	
    std::cout << "Finished" << std::endl;
	return 0;
}

Vector getColorOfRay(
	Ray ray,
	Triangle* triangles,
	int numberOfTriangles)
{
	rayBounces++;
	float red = backgroundRed;
	float green = backgroundGreen;
	float blue = backgroundBlue;

	for (int i = 0; i < numberOfTriangles; i++)
	{
		float ax = triangles[i].getVertex1().at(0);
		float ay = triangles[i].getVertex1().at(1);
		float az = triangles[i].getVertex1().at(2);
		float bx = triangles[i].getVertex2().at(0);
		float by = triangles[i].getVertex2().at(1);
		float bz = triangles[i].getVertex2().at(2);
		float cx = triangles[i].getVertex3().at(0);
		float cy = triangles[i].getVertex3().at(1);
		float cz = triangles[i].getVertex3().at(2);
		float dx = ray.getDirectionX();
		float dy = ray.getDirectionY();
		float dz = ray.getDirectionZ();
		float ex = ray.getOriginX();
		float ey = ray.getOriginY();
		float ez = ray.getOriginZ();

		Matrix3 matrix = Matrix3();
		matrix.values[0] = bx - ax;
		matrix.values[1] = by - ay;
		matrix.values[2] = bz - az;
		matrix.values[3] = cx - ax;
		matrix.values[4] = cy - ay;
		matrix.values[5] = cz - az;
		matrix.values[6] = -dx;
		matrix.values[7] = -dy;
		matrix.values[8] = -dz;

		Vector vector(
			ex - ax,
			ey - ay,
			ez - az);

		Vector uvtVector = cramersRule(
			matrix,
			vector);

		float u = uvtVector.x();
		float v = uvtVector.y();
		float t = uvtVector.z();
		if (!(v < 0 || 1 < v) &&
			!(u < 0 || 1 - v < u) &&
			!(t <= 0) &&
			!(triangles[i].getIndex() == previousTriangleIndex &&
				rayBounces > 1))
		{
			float w = 1 - u - v;
		
			float lightFactor = calculateLightFactor(
				triangles[i],
				u,
				v,
				w);
			previousTriangleIndex = triangles[i].getIndex();
			if (triangles[i].isAMirror())
			{
				Vector uVector = Vector(
					bx,
					by,
					bz);
				Vector vVector = Vector(
					cx,
					cy,
					cz);
				Vector wVector = Vector(
					ax,
					ay,
					az);
				Vector intersection =
					uVector.scale(u) +
					vVector.scale(v) +
					wVector.scale(w);
				Ray reflectedRay =
					calculateReflectedRay(
						ray,
						triangles[i],
						intersection);
				Vector rayColor = getColorOfRay(
					reflectedRay,
					triangles,
					numberOfTriangles);
				red = rayColor.x();
				green = rayColor.y();
				blue = rayColor.z();
			}
			else
			{
				red =
					triangles[i].getVertex1().at(3) * u +
					triangles[i].getVertex2().at(3) * v +
					triangles[i].getVertex3().at(3) * w;
				green =
					triangles[i].getVertex1().at(4) * u +
					triangles[i].getVertex2().at(4) * v +
					triangles[i].getVertex3().at(4) * w;
				blue =
					triangles[i].getVertex1().at(5) * u +
					triangles[i].getVertex2().at(5) * v +
					triangles[i].getVertex3().at(5) * w;
			}
			
			if (!triangles[i].isAMirror())
			{
				red = red * lightFactor;
				green = green * lightFactor;
				blue = blue * lightFactor;
			}
		}
	}
	
	rayBounces--;
	
	return Vector(
		red,
		green,
		blue);
}

Vector cramersRule(
	Matrix3 matrix,
	Vector vector)
{
	Matrix3 m0 = Matrix3();
	m0.values[0] = matrix.values[0];
	m0.values[1] = matrix.values[1];
	m0.values[2] = matrix.values[2];
	m0.values[3] = matrix.values[3];
	m0.values[4] = matrix.values[4];
	m0.values[5] = matrix.values[5];
	m0.values[6] = matrix.values[6];
	m0.values[7] = matrix.values[7];
	m0.values[8] = matrix.values[8];

	Matrix3 mx = Matrix3();
	mx.values[0] = vector.x();
	mx.values[1] = vector.y();
	mx.values[2] = vector.z();
	mx.values[3] = matrix.values[3];
	mx.values[4] = matrix.values[4];
	mx.values[5] = matrix.values[5];
	mx.values[6] = matrix.values[6];
	mx.values[7] = matrix.values[7];
	mx.values[8] = matrix.values[8];

	Matrix3 my = Matrix3();
	my.values[0] = matrix.values[0];
	my.values[1] = matrix.values[1];
	my.values[2] = matrix.values[2];
	my.values[3] = vector.x();
	my.values[4] = vector.y();
	my.values[5] = vector.z();
	my.values[6] = matrix.values[6];
	my.values[7] = matrix.values[7];
	my.values[8] = matrix.values[8];

	Matrix3 mz = Matrix3();
	mz.values[0] = matrix.values[0];
	mz.values[1] = matrix.values[1];
	mz.values[2] = matrix.values[2];
	mz.values[3] = matrix.values[3];
	mz.values[4] = matrix.values[4];
	mz.values[5] = matrix.values[5];
	mz.values[6] = vector.x();
	mz.values[7] = vector.y();
	mz.values[8] = vector.z();
	
	float determinate0 = determinate3x3(m0);
	float determinateX = determinate3x3(mx);
	float determinateY = determinate3x3(my);
	float determinateZ = determinate3x3(mz);
	
	float u = determinateX / determinate0;
	float v = determinateY / determinate0;
	float t = determinateZ / determinate0;
	
	return Vector(
		u,
		v,
		t);
}

Ray calculateReflectedRay(
	Ray ray,
	Triangle triangle,
	Vector intersection)
{
	Vector ri = Vector(
		-ray.getDirectionX(),
		-ray.getDirectionY(),
		-ray.getDirectionZ()).normalized();
	Vector n = Vector(
		triangle.getVertex1().at(6),
		triangle.getVertex1().at(7),
		triangle.getVertex1().at(8)).normalized();
		
	Vector newDirection = ri - n.scale(2).scale(1 - ri * n);
	
	newDirection = newDirection.normalized();
	
	return Ray(
		intersection.x(),
		intersection.y(),
		intersection.z(),
		newDirection.x(),
		newDirection.y(),
		newDirection.z());
}

float determinate3x3(Matrix3 matrix)
{
	float m1 = matrix.values[0];
	float m2 = matrix.values[1];
	float m3 = matrix.values[2];
	float m4 = matrix.values[3];
	float m5 = matrix.values[4];
	float m6 = matrix.values[5];
	float m7 = matrix.values[6];
	float m8 = matrix.values[7];
	float m9 = matrix.values[8];
	
	return
		m1 * m5 * m9 +
		m4 * m8 * m3 +
		m7 * m2 * m6 -
		m7 * m5 * m3 -
		m4 * m2 * m9 -
		m1 * m8 * m6;
}

bitmap_image generateImage(
	Triangle* triangles,
	std::vector<Ray> rays)
{
	bitmap_image image(
		WIDTH,
		HEIGHT);

	for (int i = 0; i < WIDTH; i++)
	{
		for (int j = 0; j < HEIGHT; j++)
		{
			Vector colorVector = getColorOfRay(
				rays.at(WIDTH * i + j),
				triangles,
				NUMBER_OF_TRIAGLES);
				
			int red = colorVector.x();
			int green = colorVector.y();
			int blue = colorVector.z();
			
			image.set_pixel(
				i,
				j,
				red,
				green,
				blue);
		}
	}
	
	return image;
}

float calculateLightFactor(
	Triangle triangle,
	float u,
	float v,
	float w)
{
	Vector uNormal = Vector(
		triangle.getVertex1().at(6),
		triangle.getVertex1().at(7),
		triangle.getVertex1().at(8)).normalized();
	Vector vNormal = Vector(
		triangle.getVertex2().at(6),
		triangle.getVertex2().at(7),
		triangle.getVertex2().at(8)).normalized();
	Vector wNormal = Vector(
		triangle.getVertex3().at(6),
		triangle.getVertex3().at(7),
		triangle.getVertex3().at(8)).normalized();
	Vector light = Vector(
		lightSourceX,
		lightSourceY,
		lightSourceZ);
	Vector smoothNormal =
		uNormal.scale(u) +
		vNormal.scale(v) +
		wNormal.scale(w);
		
	return pow(
		smoothNormal.normalized() * light.normalized(),
		PHONG_POW);
}
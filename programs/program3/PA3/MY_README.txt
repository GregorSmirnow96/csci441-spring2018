INSTRUCTIONS

--------- Overview of my classes / driver ---------

Main.cpp:
	This file contains the following configuration values at the top of the file:

	- int WIDTH (Width of final image)
	- int HEIGHT (Height of final image)
	- int NUMBER_OF_TRIANGLES (Number of triangles in scene)
	- bool orthogonal (Whether or not the rays are orthogonal)
	- int backgroundRed (Red value of the background color)
	- int backgroundGreen (Green value of the background color)
	- int backgroundBlue (Blue value of the background color)
	- float lightSourceX (X coordinate of the light source)
	- float lightSourceY (Y coordinate of the light source)
	- float lightSourceZ (Z coordinate of the light source)
	- float PHONG_POW (A constant that is used to scale the glare on objects)

	The following methods are implemented in main:

	- getColorOfRay()
	- cramersRule()
	- calculateReflectedRay()
	- determinate3x3()
	- generateImage()
	- calculateLightFactor()

	the main method uses the above methods/configurations to perform the following
	tasks in order:

	1) Create a factory for generating triangles and rays.
	2) Generates all the triangles to be rendered via the factory.
	3) Generates all the rays via the factory.
	4) Creates a bitmap_image using the rays and the triangles.
	5) Saves the image in the build folder.


Factory.h:
	The following methods are implemented in Factory.h:

	- generateTriangles()   Hard coded data for the triangles to be rendered are
				found in this method.
	- generateRays()


Ray.h:
	This class represents an encoding of a Ray.
	It contains a 3D origin and direction.


Triangle.h:
	The Triangle class contains 3 vertices and a boolean value that represents
	whether or not the triangle is a reflective surface.


Vertex.h:
	A vertex contains the data of a single point: in 3-space:

	- positionX
	- positionY
	- positionZ
	- colorRed
	- colorGreen
	- colorBlue
	- normalX
	- normalY
	- normalZ







--------- Description of my "plus 1" feature ---------

I implemented mirrors in my assignment. The concept was extremely simple, but had a few
problems during implementation.

The basic idea is to run the algorithm almost as normal; a ray's color is found by
determining the first triangle it intersects and returning the color of that triangle.
The only difference is, if the first triangle the ray hits is flagged as a mirror, the
reflected ray is calculated and recursively passed to getColorOfRay() method. The color
returned by this recursive call is assigned to the original ray.

The largest problem I ran into was, when a ray intersected a mirror and getColorOfRay()
was recursively called with the reflected ray, the point that the ray originated at would
sometimes be slightly behind the triangle that the original ray intersected. When this
happened, the ray would repeatedly intersect the same triangle and bounce off it
indefinitely until the program ran out of memory and would crash.

I fixed this issue by added an ID variable to each triangle. When a ray intersects a
triangle, the triangle's ID is logged. Any given ray is not permitted to intersect with
a triangle if the triangle's ID was the most recently logged ID. This prevented a ray
and its reflected rays from hitting the same triangle twice in a row.


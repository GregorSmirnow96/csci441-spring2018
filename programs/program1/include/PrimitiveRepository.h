#ifndef PRIMITIVEREPOSITORY_H
#define PRIMITIVEREPOSITORY_H

#include <CreationModeCLL.h>
#include <CreationModes.h>
#include <Primitive2D.h>
#include <PrimitiveCreationMode.h>
#include <PrimitiveTypes.h>

class PrimitiveRepository
{
	public:
		PrimitiveRepository(
			GLFWwindow* inWindow,
			GLuint inVBO,
			GLuint inVAO) :
			window(inWindow),
			VAO(inVAO),
			VBO(inVBO),
			primitives(new Primitive[128]),
			primitiveCount(0),
			creationMode(new PrimitiveCreationMode()) { }

		~PrimitiveRepository()
		{
		}

		void readPrimitiveCreationInput(
			Controller controller,
			int screenWidth,
			int screenHeight)
		{
			glfwGetCursorPos(window, &xPositionInit, &yPositionInit);
			creatingPrimitive = userLeftClickedInsideWindow(screenWidth, screenHeight);
			if (creatingPrimitive)
			{
				int leftButtonPressed = GetAsyncKeyState(VK_LBUTTON);
				while (leftButtonPressed < 0)
				{
					glClearColor(0.2f, 0.3f, 0.3f, 1.0f);
					glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
					creationMode->updatePrimitive(window, screenWidth, screenHeight, xPositionInit, yPositionInit, VBO, VAO, controller, customVertices, customVerticesSize);
					Primitive* primitivePointer = &(creationMode->primitive);
					primitivePointer->copyAndDescribeVertex(VBO, VAO);
					primitivePointer->render(VAO);
					renderPrimitives();
					leftButtonPressed = GetAsyncKeyState(VK_LBUTTON);
				}
				creatingPrimitive = false;
				addPrimitive(&(creationMode->primitive));
				renderPrimitives();
			}
		}

		void renderPrimitives()
		{
			for (int i = 0; i < primitiveCount; i++)
			{
				primitives[i].updateColor();
				primitives[i].copyAndDescribeVertex(VBO, VAO);
				primitives[i].render(VAO);
			}
			glfwSwapBuffers(window);
		}

		bool selectPrimitivesFromClick(
			Controller controller,
			int width,
			int height)
		{
			bool selecting = userLeftClickedInsideWindow(width, height);
			if (selecting)
			{
				while (GetAsyncKeyState(VK_LBUTTON)) { }
				glfwGetCursorPos(window, &xPositionFinal, &yPositionFinal);
				for (int i = 0; i < primitiveCount; i++)
				{
					if (primitives[i].primitiveContainsPoint(controller, xPositionFinal, height - yPositionFinal, width, height))
					{
						primitives[i].toggleSelected();
						return true;
					}
				}
			}
			return false;
		}

		void unselectAllPrimitives()
		{
			for (int i = 0; i < primitiveCount; i++)
			{
				primitives[i].setToUnselected();
			}
		}

		void saveSelectedVertices()
		{
			Primitive selectedPrimitives[64];
			int numberOfSelectedPrimitives = 0;
			for (int i = 0; i < primitiveCount; i++)
			{
				if (primitives[i].isSelected())
				{
					selectedPrimitives[numberOfSelectedPrimitives++] = primitives[i];
				}
			}
			int sizeOfNewVertices = 0;
			for (int i = 0; i < numberOfSelectedPrimitives; i++)
			{
				sizeOfNewVertices += selectedPrimitives[i].verticesSize;
			}
			customVerticesSize = sizeOfNewVertices;
			GLfloat* allVertices = new GLfloat[sizeOfNewVertices];
			int currentIndex = 0;
			for (int i = 0; i < numberOfSelectedPrimitives; i++)
			{
				int numberOfFloatsInPrimitive = selectedPrimitives[i].verticesSize / sizeof(float);
				for (int j = 0; j < numberOfFloatsInPrimitive; j++)
				{
					allVertices[currentIndex++] = selectedPrimitives[i].vertices[j];
				}
			}
			customVertices = allVertices;
		}

	private:
		Primitive * primitives;
		CreationModeCLL creationModeList;
		PrimitiveCreationMode* creationMode;
		GLFWwindow* window;
		GLuint VBO;
		GLuint VAO;
		double xPositionInit, yPositionInit, xPositionFinal, yPositionFinal;
		int primitiveCount = 0;
		int customVerticesSize = 0;
		bool creatingPrimitive;
		GLfloat* customVertices;

		// Takes in screen dimensions.
		// Returns a bool representing whether or not a user clicked inside the window.
		bool userLeftClickedInsideWindow(
			int screenWidth,
			int screenHeight)
		{
			int leftButtonPressed = GetAsyncKeyState(VK_LBUTTON);
			return (leftButtonPressed < 0 &&
					xPositionInit > 0 && xPositionInit < screenWidth &&
					yPositionInit > 0 && yPositionInit < screenHeight);
		}

		// Takes in a pointer to a Primitive.
		// Adds the primitive to PrimitiveRepository's primitives array.
		void addPrimitive(Primitive* primitive)
		{
			primitives[primitiveCount++] = *primitive;
		}

		// Necessary???
		GLfloat* combinePrimitiveVertices()
		{
			int verticesCount = 0;
			for (int i = 0; i < primitiveCount; i++)
			{
				verticesCount += primitives[i].verticesSize;
			}

			GLfloat* combinedVertices = new GLfloat[verticesCount];

			int verticeIndex = 0;
			for (int i = 0; i < primitiveCount; i++)
			{
				Primitive primitive = primitives[i];
				for (int j = 0; j < primitive.verticesSize; j++)
				{
					combinedVertices[verticeIndex++] = primitive.vertices[j];
				}
			}

			return combinedVertices;
		}
};
#endif
#ifndef PRIMITIVECREATIONMODE_H
#define PRIMITIVECREATIONMODE_H

class PrimitiveCreationMode
{
	public:
		Primitive primitive;

		PrimitiveCreationMode() { }

		virtual void updatePrimitive(
			GLFWwindow* window,
			GLfloat screenWidth,
			GLfloat screenHeight,
			GLfloat xPositionInit,
			GLfloat yPositionInit,
			GLuint VBO,
			GLuint VAO,
			Controller controller,
			GLfloat* customVertices,
			int verticesSize)
		{
			glfwGetCursorPos(window, &xPositionFinal, &yPositionFinal);
			if (controller.primitiveType == "Circle")
			{
				GLfloat xDiff = (xPositionFinal - xPositionInit) * 2 / screenWidth;
				GLfloat yDiff = (yPositionFinal - yPositionInit) * 2 / screenHeight;
				GLfloat radius = controller.scaleFactor * sqrt(xDiff * xDiff + yDiff * yDiff);
				GLfloat xCenter = controller.scaleFactor * (xPositionInit * 2 / screenWidth - 1);
				GLfloat yCenter = controller.scaleFactor * (-yPositionInit * 2 / screenHeight + 1);
				Circle circle(xCenter, yCenter, radius, 30);
				primitive = circle;
			}
			else if (controller.primitiveType == "Square")
			{
				GLfloat xInit = controller.scaleFactor * (xPositionInit * 2 / screenWidth - 1);
				GLfloat xFinal = controller.scaleFactor * (xPositionFinal * 2 / screenWidth - 1);
				GLfloat yInit = controller.scaleFactor * (-yPositionInit * 2 / screenHeight + 1);
				GLfloat yFinal = controller.scaleFactor * (-yPositionFinal * 2 / screenHeight + 1);
				GLfloat left = !(xInit<xFinal) ? xFinal : xInit;
				GLfloat right = (xInit<xFinal) ? xFinal : xInit;
				GLfloat top = !(yInit<yFinal) ? yFinal : yInit;
				GLfloat bottom = (yInit<yFinal) ? yFinal : yInit;
				Square square(left, right, top, bottom);
				primitive = square;
			}
			else if (controller.primitiveType == "Custom")
			{
				GLfloat deltaX = 1 + 2 * (xPositionFinal - xPositionInit) / screenWidth;
				GLfloat deltaY = 1 + 2 * (yPositionFinal - yPositionInit) / screenHeight;
				GLfloat* scaledVertices = new GLfloat[verticesSize];
				for (int i = 0; i < verticesSize / (6 *sizeof(float)); i++)
				{
					scaledVertices[i * 6 + 0] = controller.scaleFactor * (customVertices[i * 6 + 0] * deltaX + 2 * (xPositionInit) / screenWidth - 1);
					scaledVertices[i * 6 + 1] = controller.scaleFactor * (customVertices[i * 6 + 1] * deltaY - 2 * (yPositionInit) / screenHeight + 1);
					scaledVertices[i * 6 + 2] = customVertices[i * 6 + 2];
					scaledVertices[i * 6 + 3] = customVertices[i * 6 + 3];
					scaledVertices[i * 6 + 4] = customVertices[i * 6 + 4];
					scaledVertices[i * 6 + 5] = customVertices[i * 6 + 5];
				}
				CustomPrimitive customPrimitive(scaledVertices, verticesSize);
				primitive = customPrimitive;
			}
		}

		private:
			double xPositionFinal, yPositionFinal;
};
#endif
#ifndef PRIMITIVETYPES3D_H
#define PRIMITIVETYPES3D_H

#include <Primitive3D.h>
#include <Matrix.h>

class Cone : public Primitive
{
	public:
		Cone(
			GLfloat height,
			GLfloat radius,
			int numberOfVertsOnBase)
		{
			GLfloat* verticeArray = generateConePoints(height, radius, numberOfVertsOnBase);
			verticesSize = 144 * numberOfVertsOnBase + 36 * (numberOfVertsOnBase - 2) * sizeof(float);
			int numberOfFloats = verticesSize / sizeof(float);
			verticeRepository = new VerticeRepository(verticeArray, numberOfFloats);
			verticesSize = verticesSize * 1.5;
		}

		GLfloat* generateConePoints(
			GLfloat height,
			GLfloat radius,
			int numberOfVertsOnBase)
		{
			Matrix topOrderedPairs(numberOfVertsOnBase, 3);
			Matrix bottomOrderedPairs(numberOfVertsOnBase, 3);
			float PI = 3.14159f;
			float thetaIncrement = 360.0f / numberOfVertsOnBase;
			float theta;

			for (int i = 0; i < numberOfVertsOnBase; i++)
			{
				theta = 0.0f + i * thetaIncrement;
				float xCoord = cos(theta * PI / 180.0f) * radius;
				float yCoord = sin(theta * PI / 180.0f) * radius;
				float zCoord = height / 2.0f;
				topOrderedPairs.setElementAt(i, 0, 0);
				topOrderedPairs.setElementAt(i, 1, 0);
				topOrderedPairs.setElementAt(i, 2, zCoord);
				bottomOrderedPairs.setElementAt(i, 0, xCoord);
				bottomOrderedPairs.setElementAt(i, 1, yCoord);
				bottomOrderedPairs.setElementAt(i, 2, -zCoord);
			}

			int numberOfPointsPerFace = 36;
			float* vertices = new float[numberOfPointsPerFace * numberOfVertsOnBase
				+ 36 * (numberOfVertsOnBase - 2)];

			for (int i = 0; i < numberOfVertsOnBase; i++)
			{
				vertices[0 + i * numberOfPointsPerFace] = topOrderedPairs.getElementAt(i, 0);
				vertices[1 + i * numberOfPointsPerFace] = topOrderedPairs.getElementAt(i, 1);
				vertices[2 + i * numberOfPointsPerFace] = topOrderedPairs.getElementAt(i, 2);
				vertices[3 + i * numberOfPointsPerFace] = 1;
				vertices[4 + i * numberOfPointsPerFace] = 0;
				vertices[5 + i * numberOfPointsPerFace] = 0;
				vertices[6 + i * numberOfPointsPerFace] = topOrderedPairs.getElementAt((i + 1) % numberOfVertsOnBase, 0);
				vertices[7 + i * numberOfPointsPerFace] = topOrderedPairs.getElementAt((i + 1) % numberOfVertsOnBase, 1);
				vertices[8 + i * numberOfPointsPerFace] = topOrderedPairs.getElementAt((i + 1) % numberOfVertsOnBase, 2);
				vertices[9 + i * numberOfPointsPerFace] = 1;
				vertices[10 + i * numberOfPointsPerFace] = 0;
				vertices[11 + i * numberOfPointsPerFace] = 0;
				vertices[12 + i * numberOfPointsPerFace] = bottomOrderedPairs.getElementAt((i + 1) % numberOfVertsOnBase, 0);
				vertices[13 + i * numberOfPointsPerFace] = bottomOrderedPairs.getElementAt((i + 1) % numberOfVertsOnBase, 1);
				vertices[14 + i * numberOfPointsPerFace] = bottomOrderedPairs.getElementAt((i + 1) % numberOfVertsOnBase, 2);
				vertices[15 + i * numberOfPointsPerFace] = 1;
				vertices[16 + i * numberOfPointsPerFace] = 0;
				vertices[17 + i * numberOfPointsPerFace] = 0;


				vertices[18 + i * numberOfPointsPerFace] = bottomOrderedPairs.getElementAt((i + 1) % numberOfVertsOnBase, 0);
				vertices[19 + i * numberOfPointsPerFace] = bottomOrderedPairs.getElementAt((i + 1) % numberOfVertsOnBase, 1);
				vertices[20 + i * numberOfPointsPerFace] = bottomOrderedPairs.getElementAt((i + 1) % numberOfVertsOnBase, 2);
				vertices[21 + i * numberOfPointsPerFace] = 1;
				vertices[22 + i * numberOfPointsPerFace] = 0;
				vertices[23 + i * numberOfPointsPerFace] = 0;
				vertices[24 + i * numberOfPointsPerFace] = bottomOrderedPairs.getElementAt(i, 0);
				vertices[25 + i * numberOfPointsPerFace] = bottomOrderedPairs.getElementAt(i, 1);
				vertices[26 + i * numberOfPointsPerFace] = bottomOrderedPairs.getElementAt(i, 2);
				vertices[27 + i * numberOfPointsPerFace] = 1;
				vertices[28 + i * numberOfPointsPerFace] = 0;
				vertices[29 + i * numberOfPointsPerFace] = 0;
				vertices[30 + i * numberOfPointsPerFace] = topOrderedPairs.getElementAt(i, 0);
				vertices[31 + i * numberOfPointsPerFace] = topOrderedPairs.getElementAt(i, 1);
				vertices[32 + i * numberOfPointsPerFace] = topOrderedPairs.getElementAt(i, 2);
				vertices[33 + i * numberOfPointsPerFace] = 1;
				vertices[34 + i * numberOfPointsPerFace] = 0;
				vertices[35 + i * numberOfPointsPerFace] = 0;
			}

			//vertex to brnch off from

			for (int i = 0; i < numberOfVertsOnBase - 2; i++) {
				//top
				vertices[0 + (i + numberOfVertsOnBase) * numberOfPointsPerFace] = topOrderedPairs.getElementAt(i + 2, 0);
				vertices[1 + (i + numberOfVertsOnBase) * numberOfPointsPerFace] = topOrderedPairs.getElementAt(i + 2, 1);
				vertices[2 + (i + numberOfVertsOnBase) * numberOfPointsPerFace] = topOrderedPairs.getElementAt(i + 2, 2);
				vertices[3 + (i + numberOfVertsOnBase) * numberOfPointsPerFace] = 0;
				vertices[4 + (i + numberOfVertsOnBase) * numberOfPointsPerFace] = 1;
				vertices[5 + (i + numberOfVertsOnBase) * numberOfPointsPerFace] = 0;

				vertices[6 + (i + numberOfVertsOnBase) * numberOfPointsPerFace] = topOrderedPairs.getElementAt(i + 1, 0);
				vertices[7 + (i + numberOfVertsOnBase) * numberOfPointsPerFace] = topOrderedPairs.getElementAt(i + 1, 1);
				vertices[8 + (i + numberOfVertsOnBase) * numberOfPointsPerFace] = topOrderedPairs.getElementAt(i + 1, 2);
				vertices[9 + (i + numberOfVertsOnBase) * numberOfPointsPerFace] = 0;
				vertices[10 + (i + numberOfVertsOnBase) * numberOfPointsPerFace] = 1;
				vertices[11 + (i + numberOfVertsOnBase) * numberOfPointsPerFace] = 0;

				vertices[12 + (i + numberOfVertsOnBase) * numberOfPointsPerFace] = topOrderedPairs.getElementAt(0, 0);
				vertices[13 + (i + numberOfVertsOnBase) * numberOfPointsPerFace] = topOrderedPairs.getElementAt(0, 1);
				vertices[14 + (i + numberOfVertsOnBase) * numberOfPointsPerFace] = topOrderedPairs.getElementAt(0, 2);
				vertices[15 + (i + numberOfVertsOnBase) * numberOfPointsPerFace] = 0;
				vertices[16 + (i + numberOfVertsOnBase) * numberOfPointsPerFace] = 1;
				vertices[17 + (i + numberOfVertsOnBase) * numberOfPointsPerFace] = 0;

				//bottom
				vertices[18 + (i + numberOfVertsOnBase) * numberOfPointsPerFace] = bottomOrderedPairs.getElementAt(0, 0);
				vertices[19 + (i + numberOfVertsOnBase) * numberOfPointsPerFace] = bottomOrderedPairs.getElementAt(0, 1);
				vertices[20 + (i + numberOfVertsOnBase) * numberOfPointsPerFace] = bottomOrderedPairs.getElementAt(0, 2);
				vertices[21 + (i + numberOfVertsOnBase) * numberOfPointsPerFace] = 0;
				vertices[22 + (i + numberOfVertsOnBase) * numberOfPointsPerFace] = 0;
				vertices[23 + (i + numberOfVertsOnBase) * numberOfPointsPerFace] = 1;

				vertices[24 + (i + numberOfVertsOnBase) * numberOfPointsPerFace] = bottomOrderedPairs.getElementAt(i + 1, 0);
				vertices[25 + (i + numberOfVertsOnBase) * numberOfPointsPerFace] = bottomOrderedPairs.getElementAt(i + 1, 1);
				vertices[26 + (i + numberOfVertsOnBase) * numberOfPointsPerFace] = bottomOrderedPairs.getElementAt(i + 1, 2);
				vertices[27 + (i + numberOfVertsOnBase) * numberOfPointsPerFace] = 0;
				vertices[28 + (i + numberOfVertsOnBase) * numberOfPointsPerFace] = 0;
				vertices[29 + (i + numberOfVertsOnBase) * numberOfPointsPerFace] = 1;

				vertices[30 + (i + numberOfVertsOnBase) * numberOfPointsPerFace] = bottomOrderedPairs.getElementAt(i + 2, 0);
				vertices[31 + (i + numberOfVertsOnBase) * numberOfPointsPerFace] = bottomOrderedPairs.getElementAt(i + 2, 1);
				vertices[32 + (i + numberOfVertsOnBase) * numberOfPointsPerFace] = bottomOrderedPairs.getElementAt(i + 2, 2);
				vertices[33 + (i + numberOfVertsOnBase) * numberOfPointsPerFace] = 0;
				vertices[34 + (i + numberOfVertsOnBase) * numberOfPointsPerFace] = 0;
				vertices[35 + (i + numberOfVertsOnBase) * numberOfPointsPerFace] = 1;
			}

			return vertices;
		}
};

class Sphere : public Primitive
{
	public:
		Sphere(
			GLfloat radius,
			int vertsPerCircle)
		{
			GLfloat* verticeArray = generateSpherePoints(radius, vertsPerCircle);
			verticesSize = vertsPerCircle * vertsPerCircle * sizeof(float);
			int numberOfFloats = verticesSize / sizeof(float);
			verticeRepository = new VerticeRepository(verticeArray, numberOfFloats);
			verticesSize = verticesSize * 1.5;
		}

		GLfloat* generateSpherePoints(
			GLfloat radius,
			int vertsPerCircle)
		{
			float verticalThetaIncrement = 360.0f / vertsPerCircle;

			Matrix* nonTriangleVertices = new Matrix[vertsPerCircle * vertsPerCircle];

			for (int i = 0; i < vertsPerCircle; i++)
			{
				GLfloat theta = i * verticalThetaIncrement;
				GLfloat zCoord = cos(theta) * radius;
				for (int j = 0; j < vertsPerCircle; j++)
				{
					GLfloat phi = j * vertsPerCircle;
					GLfloat xCoord = sin(theta) * cos(phi) * radius;
					GLfloat yCoord = sin(theta) * sin(phi) * radius;
					Matrix matrix = Matrix(3, 1);
					GLfloat vertex[3] = { xCoord, yCoord, zCoord };
					matrix.populate(vertex);
					nonTriangleVertices[i * vertsPerCircle + j] = matrix;
				}
			}

			for (int i = 0; i < vertsPerCircle * vertsPerCircle; i++)
			{
				cout << nonTriangleVertices[i] << endl;
			}

			return nonTriangleVertices[0].elements;
		}
};

class Cylinder : public Primitive
{
	public:
		Cylinder(
			float radius,
			float height,
			int numberOfVerticesPerCircle)
		{
			GLfloat* verticeArray = generateCylinderPoints(radius, height, numberOfVerticesPerCircle);
			verticesSize = (36 * 256 + 36 * (256 - 2)) * sizeof(float);
			int numberOfFloats = verticesSize / sizeof(float);
			verticeRepository = new VerticeRepository(verticeArray, numberOfFloats);
			verticesSize = verticesSize * 1.5;
		}

		GLfloat* generateCylinderPoints(
			float radius,
			float height,
			int numberOfVerticesPerCircle)
		{
			Matrix topOrderedPairs(numberOfVerticesPerCircle, 3);
			Matrix bottomOrderedPairs(numberOfVerticesPerCircle, 3);
			float PI = 3.14159f;
			float thetaIncrement = 360.0f / numberOfVerticesPerCircle;
			float theta;

			for (int i = 0; i < numberOfVerticesPerCircle; i++)
			{
				theta = 0.0f + i * thetaIncrement;
				float xCoord = cos(theta * PI / 180.0f) * radius;
				float yCoord = sin(theta * PI / 180.0f) * radius;
				float zCoord = height / 2.0f;
				topOrderedPairs.setElementAt(i, 0, xCoord);
				topOrderedPairs.setElementAt(i, 1, yCoord);
				topOrderedPairs.setElementAt(i, 2, zCoord);
				bottomOrderedPairs.setElementAt(i, 0, xCoord);
				bottomOrderedPairs.setElementAt(i, 1, yCoord);
				bottomOrderedPairs.setElementAt(i, 2, -zCoord);
			}

			int numberOfPointsPerFace = 36;
			float* vertices = new float[numberOfPointsPerFace * numberOfVerticesPerCircle
				+ 36 * (numberOfVerticesPerCircle - 2)];

			for (int i = 0; i < numberOfVerticesPerCircle; i++)
			{
				vertices[0 + i * numberOfPointsPerFace] = topOrderedPairs.getElementAt(i, 0);
				vertices[1 + i * numberOfPointsPerFace] = topOrderedPairs.getElementAt(i, 1);
				vertices[2 + i * numberOfPointsPerFace] = topOrderedPairs.getElementAt(i, 2);
				vertices[3 + i * numberOfPointsPerFace] = 1;
				vertices[4 + i * numberOfPointsPerFace] = 0;
				vertices[5 + i * numberOfPointsPerFace] = 0;
				vertices[6 + i * numberOfPointsPerFace] = topOrderedPairs.getElementAt((i + 1) % numberOfVerticesPerCircle, 0);
				vertices[7 + i * numberOfPointsPerFace] = topOrderedPairs.getElementAt((i + 1) % numberOfVerticesPerCircle, 1);
				vertices[8 + i * numberOfPointsPerFace] = topOrderedPairs.getElementAt((i + 1) % numberOfVerticesPerCircle, 2);
				vertices[9 + i * numberOfPointsPerFace] = 1;
				vertices[10 + i * numberOfPointsPerFace] = 0;
				vertices[11 + i * numberOfPointsPerFace] = 0;
				vertices[12 + i * numberOfPointsPerFace] = bottomOrderedPairs.getElementAt((i + 1) % numberOfVerticesPerCircle, 0);
				vertices[13 + i * numberOfPointsPerFace] = bottomOrderedPairs.getElementAt((i + 1) % numberOfVerticesPerCircle, 1);
				vertices[14 + i * numberOfPointsPerFace] = bottomOrderedPairs.getElementAt((i + 1) % numberOfVerticesPerCircle, 2);
				vertices[15 + i * numberOfPointsPerFace] = 1;
				vertices[16 + i * numberOfPointsPerFace] = 0;
				vertices[17 + i * numberOfPointsPerFace] = 0;


				vertices[18 + i * numberOfPointsPerFace] = bottomOrderedPairs.getElementAt((i + 1) % numberOfVerticesPerCircle, 0);
				vertices[19 + i * numberOfPointsPerFace] = bottomOrderedPairs.getElementAt((i + 1) % numberOfVerticesPerCircle, 1);
				vertices[20 + i * numberOfPointsPerFace] = bottomOrderedPairs.getElementAt((i + 1) % numberOfVerticesPerCircle, 2);
				vertices[21 + i * numberOfPointsPerFace] = 1;
				vertices[22 + i * numberOfPointsPerFace] = 0;
				vertices[23 + i * numberOfPointsPerFace] = 0;
				vertices[24 + i * numberOfPointsPerFace] = bottomOrderedPairs.getElementAt(i, 0);
				vertices[25 + i * numberOfPointsPerFace] = bottomOrderedPairs.getElementAt(i, 1);
				vertices[26 + i * numberOfPointsPerFace] = bottomOrderedPairs.getElementAt(i, 2);
				vertices[27 + i * numberOfPointsPerFace] = 1;
				vertices[28 + i * numberOfPointsPerFace] = 0;
				vertices[29 + i * numberOfPointsPerFace] = 0;
				vertices[30 + i * numberOfPointsPerFace] = topOrderedPairs.getElementAt(i, 0);
				vertices[31 + i * numberOfPointsPerFace] = topOrderedPairs.getElementAt(i, 1);
				vertices[32 + i * numberOfPointsPerFace] = topOrderedPairs.getElementAt(i, 2);
				vertices[33 + i * numberOfPointsPerFace] = 1;
				vertices[34 + i * numberOfPointsPerFace] = 0;
				vertices[35 + i * numberOfPointsPerFace] = 0;
			}

			//vertex to brnch off from

			for (int i = 0; i < numberOfVerticesPerCircle - 2; i++) {
				//top
				vertices[0 + (i + numberOfVerticesPerCircle) * numberOfPointsPerFace] = topOrderedPairs.getElementAt(i + 2, 0);
				vertices[1 + (i + numberOfVerticesPerCircle) * numberOfPointsPerFace] = topOrderedPairs.getElementAt(i + 2, 1);
				vertices[2 + (i + numberOfVerticesPerCircle) * numberOfPointsPerFace] = topOrderedPairs.getElementAt(i + 2, 2);
				vertices[3 + (i + numberOfVerticesPerCircle) * numberOfPointsPerFace] = 0;
				vertices[4 + (i + numberOfVerticesPerCircle) * numberOfPointsPerFace] = 1;
				vertices[5 + (i + numberOfVerticesPerCircle) * numberOfPointsPerFace] = 0;

				vertices[6 + (i + numberOfVerticesPerCircle) * numberOfPointsPerFace] = topOrderedPairs.getElementAt(i + 1, 0);
				vertices[7 + (i + numberOfVerticesPerCircle) * numberOfPointsPerFace] = topOrderedPairs.getElementAt(i + 1, 1);
				vertices[8 + (i + numberOfVerticesPerCircle) * numberOfPointsPerFace] = topOrderedPairs.getElementAt(i + 1, 2);
				vertices[9 + (i + numberOfVerticesPerCircle) * numberOfPointsPerFace] = 0;
				vertices[10 + (i + numberOfVerticesPerCircle) * numberOfPointsPerFace] = 1;
				vertices[11 + (i + numberOfVerticesPerCircle) * numberOfPointsPerFace] = 0;

				vertices[12 + (i + numberOfVerticesPerCircle) * numberOfPointsPerFace] = topOrderedPairs.getElementAt(0, 0);
				vertices[13 + (i + numberOfVerticesPerCircle) * numberOfPointsPerFace] = topOrderedPairs.getElementAt(0, 1);
				vertices[14 + (i + numberOfVerticesPerCircle) * numberOfPointsPerFace] = topOrderedPairs.getElementAt(0, 2);
				vertices[15 + (i + numberOfVerticesPerCircle) * numberOfPointsPerFace] = 0;
				vertices[16 + (i + numberOfVerticesPerCircle) * numberOfPointsPerFace] = 1;
				vertices[17 + (i + numberOfVerticesPerCircle) * numberOfPointsPerFace] = 0;

				//bottom
				vertices[18 + (i + numberOfVerticesPerCircle) * numberOfPointsPerFace] = bottomOrderedPairs.getElementAt(0, 0);
				vertices[19 + (i + numberOfVerticesPerCircle) * numberOfPointsPerFace] = bottomOrderedPairs.getElementAt(0, 1);
				vertices[20 + (i + numberOfVerticesPerCircle) * numberOfPointsPerFace] = bottomOrderedPairs.getElementAt(0, 2);
				vertices[21 + (i + numberOfVerticesPerCircle) * numberOfPointsPerFace] = 0;
				vertices[22 + (i + numberOfVerticesPerCircle) * numberOfPointsPerFace] = 0;
				vertices[23 + (i + numberOfVerticesPerCircle) * numberOfPointsPerFace] = 1;

				vertices[24 + (i + numberOfVerticesPerCircle) * numberOfPointsPerFace] = bottomOrderedPairs.getElementAt(i + 1, 0);
				vertices[25 + (i + numberOfVerticesPerCircle) * numberOfPointsPerFace] = bottomOrderedPairs.getElementAt(i + 1, 1);
				vertices[26 + (i + numberOfVerticesPerCircle) * numberOfPointsPerFace] = bottomOrderedPairs.getElementAt(i + 1, 2);
				vertices[27 + (i + numberOfVerticesPerCircle) * numberOfPointsPerFace] = 0;
				vertices[28 + (i + numberOfVerticesPerCircle) * numberOfPointsPerFace] = 0;
				vertices[29 + (i + numberOfVerticesPerCircle) * numberOfPointsPerFace] = 1;

				vertices[30 + (i + numberOfVerticesPerCircle) * numberOfPointsPerFace] = bottomOrderedPairs.getElementAt(i + 2, 0);
				vertices[31 + (i + numberOfVerticesPerCircle) * numberOfPointsPerFace] = bottomOrderedPairs.getElementAt(i + 2, 1);
				vertices[32 + (i + numberOfVerticesPerCircle) * numberOfPointsPerFace] = bottomOrderedPairs.getElementAt(i + 2, 2);
				vertices[33 + (i + numberOfVerticesPerCircle) * numberOfPointsPerFace] = 0;
				vertices[34 + (i + numberOfVerticesPerCircle) * numberOfPointsPerFace] = 0;
				vertices[35 + (i + numberOfVerticesPerCircle) * numberOfPointsPerFace] = 1;
			}

			return vertices;
		}
};
#endif
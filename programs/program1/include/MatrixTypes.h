#ifndef MATRIXTYPES_H
#define MATRIXTYPES_H

#include <Matrix.h>

class TranslationMatrix : public Matrix
{
	public:
		TranslationMatrix(
			float x,
			float y,
			float z)
		{
			height = 4;
			width = 4;
			elements = new float[height * width];
			GLfloat matrixElements[16] = {
				1.0f, 0.0f, 0.0f,  x  ,
				0.0f, 1.0f, 0.0f,  y  ,
				0.0f, 0.0f, 1.0f,  z  ,
				0.0f, 0.0f, 0.0f, 1.0f
			};
			populate(matrixElements);
		}
};

class ScaleMatrix : public Matrix
{
	public:
		ScaleMatrix(
			GLfloat scaleX,
			GLfloat scaleY,
			GLfloat scaleZ)
		{
			height = 4;
			width = 4;
			elements = new float[height * width];
			GLfloat matrixElements[16] = {
				scaleX, 0.0f, 0.0f, 0.0f,
				0.0f, scaleY, 0.0f, 0.0f,
				0.0f, 0.0f, scaleZ, 0.0f,
				0.0f, 0.0f, 0.0f, 1.0f
			};
			populate(matrixElements);
		}
};

class RotationMatrixX : public Matrix
{
	public:
		RotationMatrixX(float theta)
		{
			height = 4;
			width = 4;
			elements = new float[height * width];
			GLfloat pi = 3.14159265f;
			float c = cos(theta * pi / 180);
			float s = sin(theta * pi / 180);

			GLfloat elements[16] = {
				c , 0.0f,  -s , 0.0f,
				0.0f, 1.0f, 0.0f, 0.0f,
				s , 0.0f,   c , 0.0f,
				0.0f, 0.0f, 0.0f, 1.0f
			};
			populate(elements);
		}
};

class RotationMatrixY : public Matrix
{
	public:
		RotationMatrixY(float theta)
		{
			height = 4;
			width = 4;
			elements = new float[height * width];
			GLfloat pi = 3.14159265f;
			float c = cos(theta * pi / 180);
			float s = sin(theta * pi / 180);

			GLfloat elements[16] = {
				1.0f, 0.0f, 0.0f, 0.0f,
				0.0f,  c  ,  -s , 0.0f,
				0.0f,  s  ,   c , 0.0f,
				0.0f, 0.0f, 0.0f, 1.0f
			};
			populate(elements);
		}
};

class RotationMatrixZ : public Matrix
{
	public:
		RotationMatrixZ(float theta)
		{
			height = 4;
			width = 4;
			elements = new float[height * width];
			GLfloat pi = 3.14159265f;
			float c = cos(theta * pi / 180);
			float s = sin(theta * pi / 180);

			GLfloat elements[16] = {
				c ,  -s , 0.0f, 0.0f,
				s ,   c , 0.0f, 0.0f,
				0.0f, 0.0f, 1.0f, 0.0f,
				0.0f, 0.0f, 0.0f, 1.0f
			};
			populate(elements);
		}
};

class OrthographicMatrix : public Matrix
{
	public:
		OrthographicMatrix(
			GLfloat l,
			GLfloat r,
			GLfloat b,
			GLfloat t,
			GLfloat n,
			GLfloat f)
		{
			height = 4;
			width = 4;
			elements = new float[height * width];
			GLfloat x1 = 2 / (r - l);
			GLfloat x2 = -((r + l) / (r - l));
			GLfloat y1 = 2 / (t - b);
			GLfloat y2 = -((t + b) / (t - b));
			GLfloat z1 = -2 / (f - n);
			GLfloat z2 = -((n + f) / (f - n));

			GLfloat matrixElements[16] = {
				x1 , 0.0f, 0.0f,  x2,
				0.0f,  y1 , 0.0f,  y2,
				0.0f, 0.0f,  z1 ,  z2,
				0.0f, 0.0f, 0.0f, 1.0f
			};
			populate(matrixElements);
		}
};

class PerspectiveMatrix : public Matrix
{
	public:
		PerspectiveMatrix(
			GLfloat l,
			GLfloat r,
			GLfloat b,
			GLfloat t,
			GLfloat n,
			GLfloat f)
		{
			height = 4;
			width = 4;
			elements = new float[height * width];
			//GLfloat x1 = (2 * abs(n)) / (r - l);
			//GLfloat y1 = (2 * abs(n)) / (t - b);
			//GLfloat z1 = (abs(n) + abs(f)) / (abs(n) - abs(f));
			//GLfloat x3 = (r + l) / (r - l);
			//GLfloat y3 = (t + b) / (t - b);
			//GLfloat z4 = (2 * (abs(n) * abs(f))) / (abs(n) - abs(f));

			GLfloat x1 = 1 / r;
			GLfloat y1 = 1 / t;
			GLfloat z1 = -2 / (f - n);
			GLfloat x3 = 0.0f;
			GLfloat y3 = 0.0f;
			GLfloat z4 = -(f + n) / (f - n);

			//GLfloat elements[16] = {
			//	x1, 0.0f, x3, 0.0f,
			//	0.0f, y1, y3, 0.0f,
			//	0.0f, 0.0f, z1, z4,
			//	0.0f, 0.0f, -1.0f, 0.0f
			//};

			GLfloat elements[16] = {
				x1, 0.0f, x3, 0.0f,
				0.0f, y1, y3, 0.0f,
				0.0f, 0.0f, z1, z4,
				0.0f, 0.0f, 0.0f, 1.0f
			};
			populate(elements);
		}
};
#endif
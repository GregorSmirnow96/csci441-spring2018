#ifndef CLLNODE_H
#define CLLNODE_H

#include <PrimitiveCreationMode.h>

class CLLNode
{
	public:
		CLLNode(PrimitiveCreationMode mode) : creationMode(&mode) { }
		
		CLLNode() { }

		CLLNode getNext()
		{
			return *nextNode;
		}

		void setNext(CLLNode node)
		{
			nextNode = &node;
		}

		PrimitiveCreationMode* getCreationMode()
		{
			return creationMode;
		}

	private:
		CLLNode* nextNode;
		PrimitiveCreationMode* creationMode;
};
#endif
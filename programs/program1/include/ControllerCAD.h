#ifndef CONTROLLER_H
#define CONTROLLER_H

class Controller
{
	public:
		string primitiveType;
		float scaleFactor;
		bool selecting;

		Controller() :
			primitiveType("Circle"),
			scaleFactor(1.0f),
			selecting(false) { }

		~Controller() {}

		void recieveUserInput()
		{
			int toggleSelecting = GetAsyncKeyState(VK_LEFT);
			int zoomIn = GetAsyncKeyState(VK_UP);
			int zoomOut = GetAsyncKeyState(VK_DOWN);
			int primitiveChange = GetAsyncKeyState(VK_SPACE);

			if (toggleSelecting)
			{
				selecting = !selecting;
				if (selecting)
				{
					cout << "\n" << endl;
					cout << "Entering 'select' mode" << endl;
				}
				else
				{
					cout << "\n" << endl;
					cout << "Selection saved" << endl;
					cout << "Entering 'draw' mode" << endl;
				}
				Sleep(500);
			}
			if (zoomIn)
			{
				scaleFactor = scaleFactor / 1.015f;
			}
			if (zoomOut)
			{
				scaleFactor = scaleFactor * 1.015f;
			}
			if (primitiveChange)
			{
				if (primitiveType == "Circle")
				{
					primitiveType = "Square";
					cout << "\n" << endl;
					cout << "Drawing: square" << endl;
				}
				else if (primitiveType == "Square")
				{
					primitiveType = "Custom";
					cout << "\n" << endl;
					cout << "Drawing: custom primitive" << endl;
				}
				else if (primitiveType == "Custom")
				{
					primitiveType = "Circle";
					cout << "\n" << endl;
					cout << "Drawing: circle" << endl;
				}
				Sleep(300);
			}
		}
};
#endif
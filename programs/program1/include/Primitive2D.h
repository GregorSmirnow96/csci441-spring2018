#ifndef PRIMITIVE_H
#define PRIMITIVE_H

// Set for 2D graphics (stride is 6) 
class Primitive
{
	public:
		GLfloat* vertices; // stride of 6. vert-vert-vert / color-color-color
		int verticesSize;
	
		Primitive() : selected(false)
		{
			float verticeArray[] = {
				-0.5f, -0.5f, 0.0f,  1.0f, 0.0f, 0.0f,
				0.5f, -0.5f, 0.0f,  0.0f, 0.0f, 1.0f,
				0.5f,  0.5f, 0.0f,  0.0f, 1.0f, 0.0f,
				0.5f,  0.5f, 0.0f,  0.0f, 1.0f, 0.0f,
				-0.5f,  0.5f, 0.0f,  0.0f, 1.0f, 1.0f,
				-0.5f, -0.5f, 0.0f,  1.0f, 0.0f, 0.0f
			};
			verticesSize = sizeof(verticeArray);
			vertices = new float[verticesSize];
			for (int i = 0; i < verticesSize; i++)
			{
				vertices[i] = verticeArray[i];
			}
		}

		~Primitive() { }

		void copyAndDescribeVertex(
			GLuint VBO,
			GLuint VAO)
		{
			// copy vertex data
			glBindBuffer(GL_ARRAY_BUFFER, VBO);
			glBufferData(GL_ARRAY_BUFFER, verticesSize, vertices, GL_DYNAMIC_DRAW);

			// describe vertex layout
			glBindVertexArray(VAO);
			glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 6 * sizeof(float),
				(void*)0);
			glEnableVertexAttribArray(0);
			glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 6 * sizeof(float),
				(void*)(3 * sizeof(float)));
			glEnableVertexAttribArray(1);
		}

		void render(GLuint VAO)
		{
			glBindVertexArray(VAO);
			glDrawArrays(GL_TRIANGLES, 0, verticesSize);
		}

		bool primitiveContainsPoint(
			Controller controller,
			int xCoord,
			int yCoord,
			int screenWidth,
			int screenHeight)
		{
			int floatsPerThreePoints = 18;
			for (int i = 0; i < verticesSize / (floatsPerThreePoints * sizeof(float)); i++)
			{
				GLfloat g1x = convertModelToViewPort(
					vertices[i * floatsPerThreePoints + 0],
					screenWidth);
				GLfloat g1y = convertModelToViewPort(
					vertices[i * floatsPerThreePoints + 1],
					screenHeight);
				GLfloat g2x = convertModelToViewPort(
					vertices[i * floatsPerThreePoints + 6],
					screenWidth);
				GLfloat g2y = convertModelToViewPort(
					vertices[i * floatsPerThreePoints + 7],
					screenHeight);
				GLfloat g3x = convertModelToViewPort(
					vertices[i * floatsPerThreePoints + 12],
					screenWidth);
				GLfloat g3y = convertModelToViewPort(
					vertices[i * floatsPerThreePoints + 13],
					screenHeight);

				GLfloat x = controller.scaleFactor * (xCoord - g1x);
				GLfloat y = controller.scaleFactor * (yCoord - g1y);
				GLfloat v0x = 0;
				GLfloat v0y = 0;
				GLfloat v1x = (g3x - g1x);
				GLfloat v1y = (g3y - g1y);
				GLfloat v2x = (g2x - g1x);
				GLfloat v2y = (g2y - g1y);
				GLfloat a = triangleAnteriorA(x, y, v0x, v0y, v1x, v1y, v2x, v2y);
				GLfloat b = triangleAnteriorB(x, y, v0x, v0y, v1x, v1y, v2x, v2y);

				bool primitiveContainsPoint = a > 0 && b > 0 && a + b < 1;
				if (primitiveContainsPoint)
				{
					return true;
				}
			}
			return false;
		}

		int convertModelToViewPort(
			GLfloat ord,
			int maxValue)
		{
			return maxValue * (ord + 1) / 2;
		}

		GLfloat triangleAnteriorA(
			GLfloat x,
			GLfloat y,
			GLfloat v0x,
			GLfloat v0y,
			GLfloat v1x,
			GLfloat v1y,
			GLfloat v2x,
			GLfloat v2y)
		{
			GLfloat arg1 = det(x, y, v2x, v2y);
			GLfloat arg2 = det(v0x, v0y, v2x, v2y);
			GLfloat arg3 = det(v1x, v1y, v2x, v2y);

			return (arg1 - arg2) / arg3;
		}

		GLfloat triangleAnteriorB(
			GLfloat x,
			GLfloat y,
			GLfloat v0x,
			GLfloat v0y,
			GLfloat v1x,
			GLfloat v1y,
			GLfloat v2x,
			GLfloat v2y)
		{
			GLfloat arg1 = det(x, y, v1x, v1y);
			GLfloat arg2 = det(v0x, v0y, v1x, v1y);
			GLfloat arg3 = det(v1x, v1y, v2x, v2y);

			return -(arg1 - arg2) / arg3;
		}

		GLfloat det(
			GLfloat v1x,
			GLfloat v1y,
			GLfloat v2x,
			GLfloat v2y)
		{
			return v1x * v2y - v1y * v2x;
		}

		void updateColor()
		{
			if (selected)
			{
				convertToGreen();
			}
			else
			{
				convertToRed();
			}
		}

		void setToSelected()
		{
			selected = true;
		}

		void setToUnselected()
		{
			selected = false;
		}

		void toggleSelected()
		{
			selected = !selected;
		}

		bool isSelected()
		{
			return selected;
		}

	private:
		bool selected;

		void convertToGreen()
		{
			int numberOfVertices = verticesSize / (6 * sizeof(float));
			for (int i = 0; i < numberOfVertices; i++)
			{
				vertices[i * 6 + 3] = 0;
				vertices[i * 6 + 4] = 1;
				vertices[i * 6 + 5] = 0;
			}
		}

		void convertToRed()
		{
			int numberOfVertices = verticesSize / (6 * sizeof(float));
			for (int i = 0; i < numberOfVertices; i++)
			{
				vertices[i * 6 + 3] = 1;
				vertices[i * 6 + 4] = 0;
				vertices[i * 6 + 5] = 0;
			}
		}
};
#endif
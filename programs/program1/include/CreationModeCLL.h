#ifndef CREATIONMODECLL_H
#define CREATIONMODECLL_H

#include <CLLNode.h>
#include <CreationModes.h>

class CreationModeCLL
{
	public:
		CreationModeCLL() : nodes(new CLLNode[2])
		{
			createNodes();
			linkNodes();
			currentNode = &nodes[0];
		}

		PrimitiveCreationMode* getCurrentMode()
		{
			return currentNode->getCreationMode();
		}

		void increment()
		{
			currentNode = &(currentNode->getNext());
		}

	private:
		CLLNode* currentNode;
		CLLNode* nodes;

		void createNodes()
		{
			CircleMode circleCreationMode = *(new CircleMode());
			SquareMode squareCreationMode = *(new SquareMode());

			CLLNode circleNode(circleCreationMode);
			CLLNode squareNode(squareCreationMode);

			nodes[0] = circleNode;
			nodes[1] = squareNode;
		}

		void linkNodes()
		{
			nodes[0].setNext(nodes[1]);
			nodes[1].setNext(nodes[0]);
		}
};
#endif
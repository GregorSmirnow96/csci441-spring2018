#ifndef PRIMITIVE_H
#define PRIMITIVE_H

#include <VerticeRepository.h>

class Primitive
{
	public:
		const int STRIDE = 9;

		VerticeRepository* verticeRepository;
		int verticesSize;
	
		Primitive()
		{
			float verticeArray[] = {
				-0.5f, -0.5f, -0.5f,  0.0f, 0.0f, 1.0f,
				0.5f, -0.5f, -0.5f,  0.0f, 0.0f, 1.0f,
				0.5f,  0.5f, -0.5f,  0.0f, 0.0f, 1.0f,
				0.5f,  0.5f, -0.5f,  0.0f, 0.0f, 1.0f,
				-0.5f,  0.5f, -0.5f,  0.0f, 0.0f, 1.0f,
				-0.5f, -0.5f, -0.5f,  0.0f, 0.0f, 1.0f,

				0.5f,  0.5f,  0.5f,  0.0f, 0.0f, 1.0f,
				0.5f, -0.5f,  0.5f,  0.0f, 0.0f, 1.0f,
				-0.5f, -0.5f,  0.5f,  0.0f, 0.0f, 1.0f,
				-0.5f, -0.5f,  0.5f,  0.0f, 0.0f, 1.0f,
				-0.5f,  0.5f,  0.5f,  0.0f, 0.0f, 1.0f,
				0.5f,  0.5f,  0.5f,  0.0f, 0.0f, 1.0f,

				-0.5f, -0.5f, -0.5f,  1.0f, 0.0f, 0.0f,
				-0.5f,  0.5f, -0.5f,  1.0f, 0.0f, 0.0f,
				-0.5f,  0.5f,  0.5f,  1.0f, 0.0f, 0.0f,
				-0.5f,  0.5f,  0.5f,  1.0f, 0.0f, 0.0f,
				-0.5f, -0.5f,  0.5f,  1.0f, 0.0f, 0.0f,
				-0.5f, -0.5f, -0.5f,  1.0f, 0.0f, 0.0f,

				0.5f,  0.5f,  0.5f,  1.0f, 0.0f, 0.0f,
				0.5f,  0.5f, -0.5f,  1.0f, 0.0f, 0.0f,
				0.5f, -0.5f, -0.5f,  1.0f, 0.0f, 0.0f,
				0.5f, -0.5f, -0.5f,  1.0f, 0.0f, 0.0f,
				0.5f, -0.5f,  0.5f,  1.0f, 0.0f, 0.0f,
				0.5f,  0.5f,  0.5f,  1.0f, 0.0f, 0.0f,

				0.5f, -0.5f,  0.5f,  0.0f, 1.0f, 0.0f,
				0.5f, -0.5f, -0.5f,  0.0f, 1.0f, 0.0f,
				-0.5f, -0.5f, -0.5f,  0.0f, 1.0f, 0.0f,
				-0.5f, -0.5f, -0.5f,  0.0f, 1.0f, 0.0f,
				-0.5f, -0.5f,  0.5f,  0.0f, 1.0f, 0.0f,
				0.5f, -0.5f,  0.5f,  0.0f, 1.0f, 0.0f,

				-0.5f,  0.5f, -0.5f,  0.0f, 1.0f, 0.0f,
				0.5f,  0.5f, -0.5f,  0.0f, 1.0f, 0.0f,
				0.5f,  0.5f,  0.5f,  0.0f, 1.0f, 0.0f,
				0.5f,  0.5f,  0.5f,  0.0f, 1.0f, 0.0f,
				-0.5f,  0.5f,  0.5f,  0.0f, 1.0f, 0.0f,
				-0.5f,  0.5f, -0.5f,  0.0f, 1.0f, 0.0f
			};
			verticesSize = sizeof(verticeArray);
			int numberOfFloats = verticesSize / sizeof(float);
			verticeRepository = new VerticeRepository(verticeArray, numberOfFloats);
			verticesSize = verticesSize * 1.5;
		}

		~Primitive()
		{
			delete verticeRepository;
		}

		void copyAndDescribeVertex(
			GLuint VBO,
			GLuint VAO)
		{
			// copy vertex data
			glBindBuffer(GL_ARRAY_BUFFER, VBO);
			glBufferData(GL_ARRAY_BUFFER, verticesSize, verticeRepository->getVertices(), GL_DYNAMIC_DRAW);

			// describe vertex layout
			glBindVertexArray(VAO);
			glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, STRIDE * sizeof(GLfloat),
				(void*)0);
			glEnableVertexAttribArray(0);
			glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, STRIDE * sizeof(GLfloat),
				(void*)(3 * sizeof(GLfloat)));
			glEnableVertexAttribArray(1);
			glVertexAttribPointer(2, 3, GL_FLOAT, GL_FALSE, STRIDE * sizeof(GLfloat),
				(void*)(6 * sizeof(GLfloat)));
			glEnableVertexAttribArray(2);
		}

		void render(GLuint VAO)
		{
			glBindVertexArray(VAO);
			glDrawArrays(GL_TRIANGLES, 0, verticesSize);
		}

};
#endif
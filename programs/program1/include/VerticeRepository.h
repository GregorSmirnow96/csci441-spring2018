#ifndef VERTICEREPOSITORY_H
#define VERTICEREPOSITORY_H

class VerticeRepository
{
	public:
		VerticeRepository(
			GLfloat* inVertices,
			int inNumberOfFloats) :
			vertices(inVertices),
			numberOfFloats(inNumberOfFloats)
		{
			addNormals();
			sortVertices();
			averageCornerNormals();
		}

		GLfloat* getVertices()
		{
			return vertices;
		}

		int getFloatCount()
		{
			return numberOfFloats;
		}

		void print()
		{
			for (int i = 0; i < numberOfFloats / 9; i++)
			{
				cout << vertices[i * 9 + 0] << ", ";
				cout << vertices[i * 9 + 1] << ", ";
				cout << vertices[i * 9 + 2] << ", ";
				cout << vertices[i * 9 + 3] << ", ";
				cout << vertices[i * 9 + 4] << ", ";
				cout << vertices[i * 9 + 5] << ", ";
				cout << vertices[i * 9 + 6] << ", ";
				cout << vertices[i * 9 + 7] << ", ";
				cout << vertices[i * 9 + 8] << endl;
			}
			cout << "-------------------------------" << endl;
		}

	private:
		GLfloat* vertices;
		int numberOfFloats;

		void addNormals()
		{
			int floatsPerNorm = 18;
			int numberOfNormals = numberOfFloats / floatsPerNorm;
			GLfloat* updatedVertices = new GLfloat[numberOfFloats * 1.5];

			for (int i = 0; i < numberOfNormals; i++)
			{
				GLfloat p1x = vertices[i * floatsPerNorm + 0];
				GLfloat p1y = vertices[i * floatsPerNorm + 1];
				GLfloat p1z = vertices[i * floatsPerNorm + 2];
				GLfloat p2x = vertices[i * floatsPerNorm + 6];
				GLfloat p2y = vertices[i * floatsPerNorm + 7];
				GLfloat p2z = vertices[i * floatsPerNorm + 8];
				GLfloat p3x = vertices[i * floatsPerNorm + 12];
				GLfloat p3y = vertices[i * floatsPerNorm + 13];
				GLfloat p3z = vertices[i * floatsPerNorm + 14];
				GLfloat points[] =
				{
					p1x, p1y, p1z,
					p2x, p2y, p2z,
					p3x, p3y, p3z
				};
				GLfloat* normal = calculateNormal(points);
				GLfloat nx = normal[0];
				GLfloat ny = normal[1];
				GLfloat nz = normal[2];
				updatedVertices[i * 27 + 0] = p1x;
				updatedVertices[i * 27 + 1] = p1y;
				updatedVertices[i * 27 + 2] = p1z;
				updatedVertices[i * 27 + 3] = vertices[i * floatsPerNorm + 3];
				updatedVertices[i * 27 + 4] = vertices[i * floatsPerNorm + 4];
				updatedVertices[i * 27 + 5] = vertices[i * floatsPerNorm + 5];
				updatedVertices[i * 27 + 6] = nx;
				updatedVertices[i * 27 + 7] = ny;
				updatedVertices[i * 27 + 8] = nz;
				updatedVertices[i * 27 + 9] = p2x;
				updatedVertices[i * 27 + 10] = p2y;
				updatedVertices[i * 27 + 11] = p2z;
				updatedVertices[i * 27 + 12] = vertices[i * floatsPerNorm + 9];
				updatedVertices[i * 27 + 13] = vertices[i * floatsPerNorm + 10];
				updatedVertices[i * 27 + 14] = vertices[i * floatsPerNorm + 11];
				updatedVertices[i * 27 + 15] = nx;
				updatedVertices[i * 27 + 16] = ny;
				updatedVertices[i * 27 + 17] = nz;
				updatedVertices[i * 27 + 18] = p3x;
				updatedVertices[i * 27 + 19] = p3y;
				updatedVertices[i * 27 + 20] = p3z;
				updatedVertices[i * 27 + 21] = vertices[i * floatsPerNorm + 15];
				updatedVertices[i * 27 + 22] = vertices[i * floatsPerNorm + 16];
				updatedVertices[i * 27 + 23] = vertices[i * floatsPerNorm + 17];
				updatedVertices[i * 27 + 24] = nx;
				updatedVertices[i * 27 + 25] = ny;
				updatedVertices[i * 27 + 26] = nz;
			}

			vertices = updatedVertices;
			numberOfFloats = numberOfFloats * 1.5;
		}

		void averageCornerNormals()
		{
			
		}
		
		void sortVertices() { cout << "Implement sortVertices()" << endl; }

		GLfloat* calculateNormal(GLfloat* threePoints)
		{
			GLfloat x1, x2, x3, y1, y2, y3, z1, z2, z3;
			GLfloat x1Vec, x2Vec, y1Vec, y2Vec, z1Vec, z2Vec;
			GLfloat xNorm, yNorm, zNorm;
			x1 = threePoints[0];
			x2 = threePoints[3];
			x3 = threePoints[6];
			y1 = threePoints[1];
			y2 = threePoints[4];
			y3 = threePoints[7];
			z1 = threePoints[2];
			z2 = threePoints[5];
			z3 = threePoints[8];
			x1Vec = x3 - x1;
			x2Vec = x2 - x1;
			y1Vec = y3 - y1;
			y2Vec = y2 - y1;
			z1Vec = z3 - z1;
			z2Vec = z2 - z1;
			xNorm = y1Vec * z2Vec - z1Vec * y2Vec;
			yNorm = z1Vec * x2Vec - x1Vec * z2Vec;
			zNorm = x1Vec * y2Vec - y1Vec * x2Vec;

			GLfloat norm[] =
			{
				xNorm,
				yNorm,
				zNorm
			};

			return normalizeVec(norm);
		}

		GLfloat* normalizeVec(GLfloat* v)
		{
			float length = sqrt(v[0] * v[0] + v[1] * v[1] + v[2] * v[2]);
			float x = v[0] / length;
			float y = v[1] / length;
			float z = v[2] / length;
			GLfloat normalizedVector[3] = { x, y, z };
			return normalizedVector;
		}
};
#endif
#ifndef STATE_H
#define STATE_H

class State
{
	public:
		State() { }

		~State() { }

		virtual void performStateActions(
			Controller controller,
			PrimitiveRepository* primitiveRepo,
			int screenWidth,
			int screenHeight) { }
};
#endif
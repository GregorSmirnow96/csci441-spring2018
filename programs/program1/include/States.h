#ifndef STATES_H
#define STATES_H

class DrawPrimitivesState : public State
{
	public:
		DrawPrimitivesState() { }
		
		~DrawPrimitivesState() { }

		void performStateActions(
			Controller controller,
			PrimitiveRepository* primitiveRepo,
			int screenWidth,
			int screenHeight)
		{
			primitiveRepo->readPrimitiveCreationInput(controller, screenWidth, screenHeight);
			primitiveRepo->renderPrimitives();
		}
};

class SelectPrimitivesState : public State
{
	public:
		SelectPrimitivesState() { }

		~SelectPrimitivesState() { }

		void performStateActions(
			Controller controller,
			PrimitiveRepository* primitiveRepo,
			int screenWidth,
			int screenHeight)
		{
			primitiveRepo->selectPrimitivesFromClick(controller, screenWidth, screenHeight);
			primitiveRepo->renderPrimitives();
			primitiveRepo->saveSelectedVertices();
		}
};
#endif
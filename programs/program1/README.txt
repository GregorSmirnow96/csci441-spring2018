----- CONROLS -----

Left arrow key	: switches between 'draw' and 'select' mode.
Up arrow key	: zoom in towards the scene
Down arrow key	: zoom away from the scene.
Space bar	: change which primitive you're drawing.



----- MODES -----

Click and drag while in 'draw' mode to draw a primitive.

Click on primitives while in 'select' mode to define a custom primitive.
The custom primitive will be saved when you exit 'select' mode.

Mode / primitive changes will be announced in the terminal.



----- BUGS ------

Sometimes, the user cannot select primitives the first time they switch
to 'select' mode. This doesn't always happen, and only ever happens once.

When zoomed in and out, the 'select' modes primitive detection isn't
accurate. (The problem is understood, but I couldn't fix it in time)
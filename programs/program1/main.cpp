#include <iostream>
#include <string>
#include <sstream>
#include <fstream>
#include <cmath>

#include <glad/glad.h>
#include <GLFW/glfw3.h>
#include <Camera.h>
#include <ControllerCAD.h>
#include <Matrix.h>
#include <MatrixTypes.h>
#include <Primitive2D.h>
#include <PrimitiveTypes.h>
#include <PrimitiveRepository.h>
#include <State.h>
#include <States.h>

#include <csci441/shader.h>

const int SCREEN_WIDTH = 640;
const int SCREEN_HEIGHT = 480;

void updateSceneScale(
	Shader shader,
	GLfloat scale);

void framebufferSizeCallback(GLFWwindow* window, int width, int height) {
	glViewport(0, 0, width, height);
}

void processInput(GLFWwindow *window, Shader &shader) {
	if (glfwGetKey(window, GLFW_KEY_ESCAPE) == GLFW_PRESS) {
		glfwSetWindowShouldClose(window, true);
	}
}

void errorCallback(int error, const char* description) {
	fprintf(stderr, "GLFW Error: %s\n", description);
}

void mouse_button_callback(GLFWwindow* window, int button, int action, int mods)
{
	if ((GetKeyState(VK_LBUTTON) & 0x100) != 0)
	{
		double xpos, ypos;
		//getting cursor position
		glfwGetCursorPos(window, &xpos, &ypos);
		cout << "Cursor Position at (" << xpos << " : " << ypos << endl;
	}
}

int main(void) {
	GLFWwindow* window;

	glfwSetErrorCallback(errorCallback);

	/* Initialize the library */
	if (!glfwInit()) { return -1; }

#ifdef __APPLE__
	glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
#endif

	/* Create a windowed mode window and its OpenGL context */
	window = glfwCreateWindow(SCREEN_WIDTH, SCREEN_HEIGHT, "Lab 4", NULL, NULL);
	if (!window) {
		glfwTerminate();
		return -1;
	}

	/* Make the window's context current */
	glfwMakeContextCurrent(window);

	// tell glfw what to do on resize
	glfwSetFramebufferSizeCallback(window, framebufferSizeCallback);

	// init glad
	if (!gladLoadGL()) {
		std::cerr << "Failed to initialize OpenGL context" << std::endl;
		glfwTerminate();
		return -1;
	}

	// create the shaders
	Shader shader("vert.glsl", "frag.glsl");

	// setup the textures
	shader.use();

	// and use z-buffering
	glEnable(GL_DEPTH_TEST);

	/* Loop until the user closes the window */
	GLuint VBO;
	glGenBuffers(1, &VBO);
	GLuint VAO;
	glGenBuffers(1, &VAO);
	Controller controller;
	Circle circle(0.0f, 0.0f, 0.0f, 60);
	double xPositionInit, yPositionInit, xPositionFinal, yPositionFinal;
	PrimitiveRepository* primitiveRepo = new PrimitiveRepository(window, VBO, VAO);
	State* drawState = new DrawPrimitivesState();
	State* selectState = new SelectPrimitivesState();
	State* currentState = selectState;

	cout << "\n" << endl;
	cout << "Drawing: circle" << endl;

	while (!glfwWindowShouldClose(window)) {
		// process input
		processInput(window, shader);

		/* Render here */
		glClearColor(0.2f, 0.3f, 0.3f, 1.0f);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		// activate shader
		shader.use();

		// create transformation matrix and pass it to shader
		updateSceneScale(shader, controller.scaleFactor);
		circle.copyAndDescribeVertex(VBO, VAO);
		circle.render(VAO);

		controller.recieveUserInput();
		if (controller.selecting)
		{
			currentState = selectState;
		}
		else
		{
			primitiveRepo->unselectAllPrimitives();
			currentState = drawState;
		}
		currentState->performStateActions(controller, primitiveRepo, SCREEN_WIDTH, SCREEN_HEIGHT);

		/* Swap front and back and poll for io events */
		glfwPollEvents();

		Sleep(17);
	}

	glfwTerminate();
	return 0;
}

void updateSceneScale(
	Shader shader,
	GLfloat scale)
{
	GLint shaderVariableIndex =
		glGetUniformLocation(
			shader.id(),
			"scaleFactor");
	glUniform1f(
		shaderVariableIndex,
		scale);
}
#version 330 core
layout (location = 0) in vec3 aPos;
layout (location = 1) in vec3 aColor;

out vec3 ourColor;

uniform float scaleFactor;

void main()
{
	vec3 scaledPos = vec3(aPos[0] / scaleFactor, aPos[1] / scaleFactor, aPos[2] / scaleFactor);
	gl_Position = vec4(scaledPos, 1);
	ourColor = aColor;
}
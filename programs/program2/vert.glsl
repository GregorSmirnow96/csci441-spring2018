#version 330 core
layout (location = 0) in vec3 aPos;
layout (location = 1) in vec3 aColor;

out vec3 ourColor;

uniform mat4 modelMatrix;

vec4 getNewPos(mat4 m)
{
	//int projectionMode = 0;
	//if(projectionMode == 0){
		vec4 pos = vec4(aPos, 1);
		float x = m[0][0] * pos[0] + m[0][1] * pos[1] + m[0][2] * pos[2] + m[0][3] * pos[3];
		float y = m[1][0] * pos[0] + m[1][1] * pos[1] + m[1][2] * pos[2] + m[1][3] * pos[3];
		float z = m[2][0] * pos[0] + m[2][1] * pos[1] + m[2][2] * pos[2] + m[2][3] * pos[3];
		return vec4(x, y, z, 1);
	//}
	//if(projectionMode == 1){
	//	vec4 pos = vec4(aPos, 1);
	//	float x = m[0][0] * pos[0] + m[0][1] * pos[1] + m[0][2] * pos[2] + m[0][3] * pos[3];
	//	float y = m[1][0] * pos[0] + m[1][1] * pos[1] + m[1][2] * pos[2] + m[1][3] * pos[3];
	//	float z = m[2][0] * pos[0] + m[2][1] * pos[1] + m[2][2] * pos[2] + m[2][3] * pos[3];
	//	float w = m[3][0] * pos[0] + m[3][1] * pos[1] + m[3][2] * pos[2] + m[3][3] * pos[3];
	//	return vec4(x, y, z, w);
	//}
}

void main()
{
	vec4 newPos = getNewPos(modelMatrix);
	gl_Position = vec4(newPos);
	ourColor = aColor;
}
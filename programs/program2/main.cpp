#include <iostream>
#include <string>
#include <sstream>
#include <fstream>
#include <cmath>

#include <vector.h>
#include <glad/glad.h>
#include <GLFW/glfw3.h>
#include <Camera.h>
#include <Controller.h>
#include <DataStructures/Mesh.h>
#include <DataStructures/LinkedList.h>
#include <Matrix.h>
#include <MatrixTypes.h>
#include <PrimitiveStructure/Primitive.h>
#include <PrimitiveStructure/PrimitiveTypes.h>
#include <PrimitiveStructure/PrimitiveRepository.h>

#include <csci441/shader.h>

const int SCREEN_WIDTH = 640;
const int SCREEN_HEIGHT = 480;
const int PHONG_POW = 4096;
const GLfloat LIGHT_X = 1.0f;
const GLfloat LIGHT_Y = 0.0f;
const GLfloat LIGHT_Z = 0.0f;

void updateModelToProjectionMatrix(
	Shader shader,
	Controller controller);
void updateEyePosition(
	Shader shader,
	Controller controller);
void updateModelMatrix(
	Shader shader,
	Controller controller);
void updateProjectionMode(
	Shader shader,
	int mode);
void createLightSource(Shader shader);
void updatePhongPow(Shader shader);
int* generateMazeData(
	string fileName,
	int fileSize);
void generateMaze(
	Shader* mazeShader,
	int mazeDimension,
	string fileName,
	PrimitiveRepository* primitiveRepository);
Primitive* initializePlayer(
	Shader* playerShader,
	PrimitiveRepository* primitiveRepository,
	int mazeDimension,
	float x,
	float y,
	float z);

void framebufferSizeCallback(GLFWwindow* window, int width, int height) {
	glViewport(0, 0, width, height);
}

void processInput(GLFWwindow *window) {
	if (glfwGetKey(window, GLFW_KEY_ESCAPE) == GLFW_PRESS) {
		glfwSetWindowShouldClose(window, true);
	}
}

void errorCallback(int error, const char* description) {
	fprintf(stderr, "GLFW Error: %s\n", description);
}

int main(void)
{
	GLFWwindow* window;

	glfwSetErrorCallback(errorCallback);

	/* Initialize the library */
	if (!glfwInit()) { return -1; }

#ifdef __APPLE__
	glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
#endif

	/* Create a windowed mode window and its OpenGL context */
	window = glfwCreateWindow(SCREEN_WIDTH, SCREEN_HEIGHT, "Shading Lab", NULL, NULL);
	if (!window) {
		glfwTerminate();
		return -1;
	}

	/* Make the window's context current */
	glfwMakeContextCurrent(window);

	// tell glfw what to do on resize
	glfwSetFramebufferSizeCallback(window, framebufferSizeCallback);

	// init glad
	if (!gladLoadGL()) {
		std::cerr << "Failed to initialize OpenGL context" << std::endl;
		glfwTerminate();
		return -1;
	}

	PrimitiveRepository* primitiveRepository = &PrimitiveRepository();

	Shader shader(
		"vert.glsl",
		"frag.glsl");

	generateMaze(
		&shader,
		10,
		"mazeRepresentations/mediumMazeData.txt",
		primitiveRepository);

	primitiveRepository->loadFromObjFile(
		"objFiles/maze.obj",
		&shader);

	//primitiveRepository->writeToObjFile("objFiles/maze.obj");

	initializePlayer(
		&shader,
		primitiveRepository,
		10,
		0.96f,
		0.70f,
		0.9f);

	// and use z-buffering
	glEnable(GL_DEPTH_TEST);

	// initialize light source
		// createLightSource(shader);

	// pass Phong POW!!!!!
		// updatePhongPow(shader);

	//primitive->verticeRepository->print();
	int numberOfPrimitives = primitiveRepository->primitives->getSize();
	for (int i = 0; i < numberOfPrimitives - 1; i++)
	{
		primitiveRepository->primitives->elementAt(i).controller->yRotationDegree = -90;
	}
	Controller* playerController = primitiveRepository->primitives->elementAt(numberOfPrimitives - 1).controller;

	/* Loop until the user closes the window */
	while (!glfwWindowShouldClose(window)) {
		// process input
		processInput(window);

		/* Render here */
		glClearColor(0.2f, 0.3f, 0.3f, 1.0f);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		// read user input
		playerController->recieveUserInput();
		
		// render the scene
		primitiveRepository->renderAllPrimitives();

		/* Swap front and back and poll for io events */
		glfwSwapBuffers(window);
		glfwPollEvents();
	}

	glfwTerminate();
	return 0;
}

void createLightSource(Shader shader)
{
	GLint shaderVariableIndex =
		glGetUniformLocation(
			shader.id(),
			"lightPos");
	GLfloat lightDirection[] =
	{
		LIGHT_X,
		LIGHT_Y,
		LIGHT_Z
	};
	glUniform3f(
		shaderVariableIndex,
		lightDirection[0],
		lightDirection[1],
		lightDirection[2]);

	shaderVariableIndex =
		glGetUniformLocation(
			shader.id(),
			"lightIntensity");
	GLfloat lightIntensity[] = {
		1.0f,
		1.0f,
		1.0f };
	glUniform3f(
		shaderVariableIndex,
		lightIntensity[0],
		lightIntensity[1],
		lightIntensity[2]);
}

void updateEyePosition(
	Shader shader,
	Controller controller)
{
	GLint shaderVariableIndex =
		glGetUniformLocation(
			shader.id(),
			"eyePos");
	GLfloat eyePosition[] = {
		controller.cameraLocation->x(),
		controller.cameraLocation->y(),
		controller.cameraLocation->z() };
	glUniform3f(
		shaderVariableIndex,
		eyePosition[0],
		eyePosition[1],
		eyePosition[2]);
}

void updateProjectionMode(
	Shader shader,
	int mode)
{
	//mode = 0 => orthographic
	//mode = 1 => perspective
	GLint shaderVariableIndex =
		glGetUniformLocation(
			shader.id(),
			"projectionMode");
	glUniform1i(
		shaderVariableIndex,
		mode);
}


void updateModelToProjectionMatrix(
	Shader shader,
	Controller controller)
{
	OrthographicMatrix orthMatrix(-1.0f, 1.0f, -1.0f, 1.0f, 1.0f, 50.0f);
	PerspectiveMatrix perspMatrix(-1.0f, 1.0f, -1.0f, 1.0f, 1.0f, 50.0f);
	TranslationMatrix eyeSpaceMatrix(0.0f, 0.0f, controller.cameraLocation->z());

	Matrix* projection;
	if (controller.projectionMode == 0) {
		Matrix m = orthMatrix;
		projection = &m;
	}
	else {
		Matrix m = perspMatrix;
		projection = &m;
	}
	Matrix n = *projection * eyeSpaceMatrix;

	n.passToShader(
		shader,
		"cameraTransformMatrix");
}

void updateModelMatrix(
	Shader shader,
	Controller controller)
{
	TranslationMatrix translateMatrix(
		controller.modelTranslateX,
		controller.modelTranslateY,
		controller.modelTranslateZ);
	ScaleMatrix scaleMatrix(
		controller.modelScale,
		controller.modelScale,
		controller.modelScale);
	RotationMatrixX x(controller.xRotationDegree);
	RotationMatrixY y(controller.yRotationDegree);
	RotationMatrixZ z(controller.zRotationDegree);

	Matrix* m = &(y * x);
	m = &(z * *m);
	m = &(scaleMatrix * *m);
	m = &(translateMatrix * *m);

	m->passToShader(
		shader,
		"modelMatrix");
}

void updatePhongPow(Shader shader)
{
	GLint shaderVariableIndex =
		glGetUniformLocation(
			shader.id(),
			"phongPow");
	GLint phongPow = PHONG_POW;
	glUniform1i(
		shaderVariableIndex,
		phongPow);
}

void generateMaze(
	Shader* shader,
	int mazeDimension,
	string fileName,
	PrimitiveRepository* primitiveRepository)
{
	int fileSize = mazeDimension * mazeDimension;
	int* mazeData = generateMazeData(
		fileName,
		fileSize);
	const float cubeDimension = 2.0f / (float) mazeDimension;
	for (int i = 0; i < mazeDimension; i++)
	{
		for (int j = 0; j < mazeDimension; j++)
		{
			if (mazeData[i * mazeDimension + j] == 1)
			{
				Cube cube = Cube(shader);
				primitiveRepository->addPrimitive(&cube);
				cube.generateVertices(
					-1.0f + j * cubeDimension,
					-1.0f + (j + 1) * cubeDimension,
					1.0f,
					-1.0f,
					1.0f - i * cubeDimension,
					1.0f - (i + 1) * cubeDimension);
				cube.copyAndDescribeVertex();
			}
		}
	}
}

int* generateMazeData(
	string fileName,
	int fileSize)
{
	ifstream inFile;
	inFile.open(fileName);
	char x;
	int arrayIndex = 0;
	int* mazeData = new int[fileSize];
	while (inFile >> x)
	{
		if (x == 'X')
		{
			mazeData[arrayIndex++] = 1;
		}
		else
		{
			mazeData[arrayIndex++] = 0;
		}
	}
	return mazeData;
}

Primitive* initializePlayer(
	Shader* shader,
	PrimitiveRepository* primitiveRepository,
	int mazeDimension,
	float x,
	float y,
	float z)
{
	float playerDimension = (float) 1 / (mazeDimension * 2);
	Cube cube = Cube(shader);
	cube.generateVertices(
		x - playerDimension,
		x + playerDimension,
		y - playerDimension,
		y + playerDimension,
		z + playerDimension,
		z - playerDimension,
		1,
		0,
		0);
	cube.copyAndDescribeVertex();
	primitiveRepository->addPrimitive(&cube);

	Primitive* player = &cube;

	return player;
}
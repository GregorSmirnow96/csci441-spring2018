PROGRAMMING ASSIGNMENT 2

Implemented Features:
	- Saving models to .obj files
	- Reading in models from .obj files
	- Object oriented approach at representing models components
	- Bird's eye view of maze / navigation


Description of User Controls:
	User can move around in bird's eye view with the arrow keys.


Description of My Custom Model Library:
	Models are created and stored using the following classes:
		- PrimitiveRepository
		- Primitive
		- PolygonFace
		- Vertice
		- Attribute

	PrimitiveRepository is a composition of 0..* Primitives.
	Primitive is a composition of 0..* PolygonFaces and 0..* Vertices.
	PolygonFace is a composition of 3 Vertices.
	Vertice is a composition of 0..* Attributes.
	An instance of Attribute represents one aspect of a vertex (color, location, etc.).
	
	All of the above classes implement my IRepository interface. This interface has 2 methods:
		- float* collectElementsAsListOfFloats()
		- int numberOfFloatsInData()

	To get the float array representaion of a primitive, Primitive.collectElementsAsFloatArray
	is called. This, in turn, calls its subcomponents' collectElementsAsFloatArray methods (since
	all components implement IRepository). The size of the resulting list is calculated in the
	same fashion via Primitive.numberOfFloatsInData().

	A model that is made up of many smaller primitives can be saved as an obj by adding all
	primitives that make up the larger primitive to a PrimitiveRepository and calling
	PrimitiveRepository.writeToObjFile(). In turn, PrimitiveRepository can load in a model
	via an obj file with PrimitiveRepository.loadFromObjFile().


Strengths:
	The PrimitiveRepository architecture largely reduced the complexity of model management (at
	the cost of an unexpectedly large amount of implementation time at the start of the
	assignment).

	Mazes can be generated from text files that contain 'X's and '.'s, and then saved as a
	single primitive via main's generateMaze() method. Example files can be viewed in the
	program2/mazeRepresentations folder. 'X's represent walls while '.'s represent paths.

Weaknesses:
	I had planned for the PrimitiveRepository to save time, but it ended up costing me ~12 hours.

	Due to the time sink for devloping my PrimitiveRepository, I didn't finish implementing the
	camera view of the maze. Only bird's eye view is available.

	For some obj files, PrimitiveRepository.loadFromObjFile() will correctly load in the data
	(I know this because I examined the data via Visual Studio's debugger at runtime), but the
	object will not render.